!! This module has been extensively checked mathematically and fairly well tested.
!! Don't change the math unless you are absolutely certain since you may be introducing
!! a bug.

!! This should be a public module because these routines may be useful for other equation
!! of state software.  

module thermo
  use watereos_def, only: dp
  implicit none
  
contains
  
  !! These two functions below are actually the same, but with different
  !! symbols (X,Y,Z) is a well defined relation and other functions F_i(X,Y)
  !! then you can convert first and second derivatives of Z(X,Y), and F_i(X,Y)
  !!   to first and second derivatives of X(Z,Y) and F_i(Z,Y)
  !! this function then is the general format.  The first row is the X dimension
  !! the second row is the Y dimension
  !! the third row is the Z dimension
  !! the rows after that are F_i
  !! size should be 3 + n(i)
  subroutine convXY_ZY(in_matrix, out_matrix, size)
    use watereos_def

    real(dp), dimension(:,:), intent(in) :: in_matrix
    real(dp), dimension(:,:), intent(out) :: out_matrix
    integer, intent(in) :: size
    real(dp) :: conv(num_derivs,num_derivs) !! This matrix will do all the work
    integer :: i,j,k
    integer :: Zi = 3

    out_matrix(:,:) = 0.d0
    conv(:,:) = 0.d0
    conv(i_val,i_val) = 1.
    conv(i_dZ,i_dX) = 1./in_matrix(Zi,i_dX)
    conv(i_dY,i_dX) = -in_matrix(Zi,i_dY)/in_matrix(Zi,i_dX)
    conv(i_dY,i_dY) = 1.
    conv(i_dZ2,i_dX) = -in_matrix(Zi,i_dX2)/in_matrix(Zi,i_dX)**3
    conv(i_dZ2,i_dX2) = 1./in_matrix(Zi,i_dX)**2
    conv(i_dY2,i_dX) = (2.*in_matrix(Zi,i_dY)*in_matrix(Zi,i_dXdY)/in_matrix(Zi,i_dX)**2 &
                        - in_matrix(Zi,i_dY)**2 * in_matrix(Zi,i_dX2)/in_matrix(Zi, i_dX)**3 &
                        - in_matrix(Zi,i_dY2)/in_matrix(Zi,i_dX))
    conv(i_dY2,i_dX2) = (in_matrix(Zi,i_dY)/in_matrix(Zi,i_dX))**2
    conv(i_dY2,i_dY2) = 1.
    conv(i_dY2,i_dXdY) = -2.*in_matrix(Zi,i_dY)/in_matrix(Zi,i_dX)
    conv(i_dZdY,i_dX) = -in_matrix(Zi,i_dXdY)/in_matrix(Zi,i_dX)**2 &
                        + in_matrix(Zi,i_dY)*in_matrix(Zi,i_dX2)/in_matrix(Zi,i_dX)**3
    conv(i_dZdY,i_dX2) = -in_matrix(Zi,i_dY)/in_matrix(Zi,i_dX)**2
    conv(i_dZdY,i_dXdY) = 1./in_matrix(Zi,i_dX)
    
    
    do i=1,size
       do j=1,num_derivs
          do k=1,num_derivs
             out_matrix(i,j) = out_matrix(i,j) + conv(j,k)*in_matrix(i,k)
          end do
       end do
    enddo
  end subroutine convXY_ZY
  

  !! This can be treated as Rho, T, P, Pvect, Evect, Svect, Rhovect, Evect, Svect
  subroutine convRhoT_PT(Rho, T, P, &
       Pvect_input, Evect_input, Svect_input, &
       Rhovect_output, Evect_output, Svect_output)
    use watereos_def

    !! (Rho, T) --> (P, T)
    real(dp), intent(in) :: Rho, T, P
    real(dp), intent(in) :: Pvect_input(num_derivs), Evect_input(num_derivs), Svect_input(num_derivs)
    real(dp), intent(out) :: Rhovect_output(num_derivs), Evect_output(num_derivs), Svect_output(num_derivs)
    real(dp) :: data_matrix1(5,num_derivs), data_matrix2(5,num_derivs)
    
    data_matrix1(:,:) = 0.
    data_matrix1(1,i_val) = Rho
    data_matrix1(1,i_dRho) = 1d0
    data_matrix1(2,i_val) = T
    data_matrix1(2,i_dT) = 1d0
    data_matrix1(3,:) = Pvect_input
    data_matrix1(4,:) = Evect_input
    data_matrix1(5,:) = Svect_input
    
    call convXY_ZY(data_matrix1, data_matrix2, 5)
    Rhovect_output = data_matrix2(1,:)
    Evect_output = data_matrix2(4,:)
    Svect_output = data_matrix2(5,:)
  end subroutine convRhoT_PT

  subroutine convPT_RhoT(Rho, T, P, &
       Rhovect_input, Evect_input, Svect_input, &
       Pvect_output, Evect_output, Svect_output)
    use watereos_def

    !! (P, T) --> (Rho, T)
    real(dp), intent(in) :: Rho, T, P
    real(dp), intent(in) :: Rhovect_input(num_derivs), Evect_input(num_derivs), Svect_input(num_derivs)
    real(dp), intent(out) :: Pvect_output(num_derivs), Evect_output(num_derivs), Svect_output(num_derivs)
    real(dp) :: data_matrix1(5,num_derivs), data_matrix2(5,num_derivs)
    
    data_matrix1(:,:) = 0.
    data_matrix1(1,i_val) = P
    data_matrix1(1,i_dP) = 1.
    data_matrix1(2,i_val) = T
    data_matrix1(2,i_dT) = 1.
    data_matrix1(3,:) = Rhovect_input
    data_matrix1(4,:) = Evect_input
    data_matrix1(5,:) = Svect_input

    call convXY_ZY(data_matrix1, data_matrix2, 5)
    Pvect_output = data_matrix2(1,:)
    Evect_output = data_matrix2(4,:)
    Svect_output = data_matrix2(5,:)
  end subroutine convPT_RhoT

  !! This subroutine will convert the outputs from the results structure in the standard "eos_def.f" format
  !! to the straight up derivatives format that is used above
  !! and generally used in mesa.  This is generally useful because we are adding things in the general
  !! derivatives space and then we construct various thermodynamic quantities using these derivatives.
  subroutine convRES_lnderivs(res, d_dlnRho, d_dlnT, lnRho, lnT, lnPvect, lnEvect, lnSvect)
    use eos_def
    use watereos_def

    real(dp), intent(in) :: res(num_eos_basic_results), d_dlnRho(num_eos_basic_results), &
                                    d_dlnT(num_eos_basic_results)
    real(dp), intent(in) :: lnRho, lnT
    real(dp), intent(out) :: lnPvect(num_derivs), lnEvect(num_derivs), lnSvect(num_derivs)
    real(dp) :: d, S, E, T, P
    
    d = exp(lnRho)
    T = exp(lnT)
    P = exp(res(i_lnPgas))
    S = exp(res(i_lnS))
    E = exp(res(i_lnE))
    
    lnPvect(i_val) = res(i_lnPgas)
    lnPvect(i_dlnRho) = d_dlnRho(i_lnPgas)
    lnPvect(i_dlnT) = d_dlnT(i_lnPgas)
    lnPvect(i_dlnRho2) = d_dlnRho(i_chiRho)
    lnPvect(i_dlnT2) = d_dlnT(i_chiT)
    lnPvect(i_dlnRhodlnT) = d_dlnRho(i_chiT)

    lnSvect(i_val) = res(i_lnS)
    lnSvect(i_dlnRho) = d_dlnRho(i_lnS)
    lnSvect(i_dlnT) = d_dlnT(i_lnS)
    lnSvect(i_dlnRho2) = (d/S) * d_dlnRho(i_dS_dRho) - d_dlnRho(i_lnS)**2d0 + d_dlnRho(i_lnS)
    lnSvect(i_dlnT2) =   (T/S) * d_dlnT(i_dS_dT)    - d_dlnT(i_lnS)**2d0    + d_dlnT(i_lnS)
    lnSvect(i_dlnRhodlnT) = (d/S) * d_dlnT(i_dS_dRho) - d_dlnRho(i_lnS) * d_dlnT(i_lnS)

    lnEvect(i_val) = res(i_lnE)
    lnEvect(i_dlnRho) = d_dlnRho(i_lnE)
    lnEvect(i_dlnT) = d_dlnT(i_lnE)
    lnEvect(i_dlnRho2) = (d/E) * d_dlnRho(i_dE_dRho) - d_dlnRho(i_lnE)**2d0 +  d_dlnRho(i_lnE)
    lnEvect(i_dlnT2) =   (T/E) * d_dlnT(i_Cv)        - d_dlnT(i_lnE)**2d0   + d_dlnT(i_lnE)
    lnEvect(i_dlnRhodlnT) = (d/E) * d_dlnT(i_dE_dRho) - d_dlnRho(i_lnE) * d_dlnT(i_lnE)

  end subroutine convRES_lnderivs
  
  !! This is the inverse routine
  subroutine convlnderivs_RES(lnRho, lnT, lnPvect, lnEvect, lnSvect, res, d_dlnRho_c_T, d_dlnT_c_Rho)
    use eos_def
    use watereos_def

    real(dp), intent(in) :: lnRho, lnT
    real(dp), intent(in) :: lnPvect(num_derivs), lnEvect(num_derivs), lnSvect(num_derivs)
    real(dp), intent(out) :: res(num_eos_basic_results), d_dlnRho_c_T(num_eos_basic_results), &
                                     d_dlnT_c_Rho(num_eos_basic_results)
    
    real(dp) :: alpha, beta, Cv, Cp
    real(dp) :: lnP,lnE,lnS,P,E,S,d,T
    real(dp) :: dlnEdlnRho_ct,dlnEdlnT_cd, &
         dlnSdlnRho_ct, dlnSdlnT_cd,&
         dlnPdlnRho_ct,dlnPdlnT_cd
    real(dp) :: d2lnSdlnT2_cd, d2lnSdlnRhodlnT, d2lnSdlnRho2_ct, &
         d2lnEdlnRho2_ct,d2lnEdlnRhodlnT, d2lnEdlnT2_cd, &
         d2lnPdlnRho2_ct,d2lnPdlnRhodlnT, d2lnPdlnT2_cd,&
         dalphadlnRho_ct, dalphadlnT_cd,&
         dbetadlnRho_ct, dbetadlnT_cd,&
         dCvdlnRho_ct, dCvdlnT_cd,&
         dCpdlnRho_ct, dCpdlnT_cd
    
    res(:) = 0.
    d_dlnRho_c_T(:) = 0.
    d_dlnT_c_Rho(:) = 0.
    
    lnP = lnPvect(i_val)
    lnE = lnEvect(i_val)
    lnS = lnSvect(i_val)
    
    dlnPdlnRho_ct = lnPvect(i_dlnRho)
    dlnPdlnT_cd = lnPvect(i_dlnT)
    d2lnPdlnRho2_ct = lnPvect(i_dlnRho2)
    d2lnPdlnT2_cd = lnPvect(i_dlnT2)
    d2lnPdlnRhodlnT = lnPvect(i_dlnRhodlnT)
    
    dlnEdlnRho_ct = lnEvect(i_dlnRho)
    dlnEdlnT_cd = lnEvect(i_dlnT)
    d2lnEdlnRho2_ct = lnEvect(i_dlnRho2)
    d2lnEdlnT2_cd = lnEvect(i_dlnT2)
    d2lnEdlnRhodlnT = lnEvect(i_dlnRhodlnT)
    
    dlnSdlnRho_ct = lnSvect(i_dlnRho)
    dlnSdlnT_cd = lnSvect(i_dlnT)
    d2lnSdlnRho2_ct = lnSvect(i_dlnRho2)
    d2lnSdlnT2_cd = lnSvect(i_dlnT2)
    d2lnSdlnRhodlnT = lnSvect(i_dlnRhodlnT)
    
    d = exp(lnRho)
    T = exp(lnT)
    P = exp(lnP)
    E = exp(lnE)
    S = exp(lnS)
    
    Cv = (E/T) * dlnEdlnT_cd
    dCvdlnRho_ct = (E / T) * dlnEdlnRho_ct * dlnEdlnT_cd &
         + (E / T) * d2lnEdlnRhodlnT
    dCvdlnT_cd = (E / T) * dlnEdlnT_cd**2 &
         - (E / T) * dlnEdlnT_cd + (E/T) * d2lnEdlnT2_cd
    
    alpha = dlnPdlnT_cd / (T*dlnPdlnRho_ct)
    dalphadlnRho_ct = d2lnPdlnRhodlnT / (T*dlnPdlnRho_ct) &
         - (dlnPdlnT_cd*d2lnPdlnRho2_ct) / (T*dlnPdlnRho_ct**2)
    dalphadlnT_cd = d2lnPdlnT2_cd / (T * dlnPdlnRho_ct) &
         - alpha - (dlnPdlnT_cd * d2lnPdlnRhodlnT) / (T * dlnPdlnRho_ct**2)
    
    beta = 1/(P * dlnPdlnRho_ct)
    dbetadlnRho_ct = - dlnPdlnRho_ct / (P * dlnPdlnRho_ct) &
         - d2lnPdlnRho2_ct / (P * dlnPdlnRho_ct)
    dbetadlnT_cd = - dlnPdlnT_cd / (P * dlnPdlnRho_ct) &
         - d2lnPdlnRhodlnT / (P * dlnPdlnRho_ct**2)
    
!    write(*,*) "[thermo]"
!    write(*,*) "dlnPdlnRho_ct: ", dlnPdlnRho_ct
!    write(*,*) "dlnPdlnT_cd: ", dlnPdlnT_cd
!    write(*,*) "dlnEdlnRho_ct: ", dlnEdlnRho_ct
!    write(*,*) "dlnEdlnT_cd: ", dlnEdlnT_cd
!    write(*,*) "d: ", d
!    write(*,*) "T: ", T
!    write(*,*) "P: ", P
!    write(*,*) "E: ", E


    Cp = (- (d/T)*dlnPdlnT_cd/dlnPdlnRho_ct)&
              *(E/d*dlnPdlnRho_ct+P/d**2d0*dlnPdlnRho_ct-P/d**2d0)&
          +E/T*dlnEdlnT_cd + P/(d*T)*dlnPdlnT_cd
    dCpdlnRho_ct = -(E/T)*dlnEdlnRho_ct*dlnPdlnT_cd*dlnEdlnRho_ct/dlnPdlnRho_ct &
                   -(E/T)*d2lnPdlnRhodlnT*dlnEdlnRho_ct/dlnPdlnRho_ct&
                   -(E/T)*dlnPdlnT_cd*d2lnEdlnRho2_ct/dlnPdlnRho_ct&
                   +(E/T)*dlnPdlnT_cd*dlnEdlnRho_ct*d2lnPdlnRho2_ct/dlnPdlnRho_ct**2&
                   -(P/(d*T))*dlnPdlnRho_ct*dlnPdlnT_cd &
                   -(P/(d*T))*d2lnPdlnRhodlnT &
                   +(P/(d*T))*dlnPdlnT_cd&
                   +(P/(d*T))*dlnPdlnT_cd&
                   +(P/(d*T))*d2lnPdlnRhodlnT/dlnPdlnRho_ct&
                   -(P/(d*T))*dlnPdlnT_cd/dlnPdlnRho_ct&
                   -(P/(d*T))*dlnPdlnT_cd*d2lnPdlnRho2_ct/dlnPdlnRho_ct**2&
                   +(E/T)*dlnEdlnRho_ct*dlnEdlnT_cd&
                   +(E/T)*d2lnEdlnRhodlnT&
                   +(P/(d*T))*dlnPdlnRho_ct*dlnPdlnT_cd&
                   +(P/(d*T))*d2lnPdlnRhodlnT&
                   -(P/(d*T))*dlnPdlnT_cd !! See page 80 of notebook    
    dCpdlnT_cd =  -(E/T)*dlnEdlnT_cd*dlnPdlnT_cd*dlnEdlnRho_ct/dlnPdlnRho_ct&
                  -(E/T)*d2lnPdlnT2_cd*dlnEdlnRho_ct/dlnPdlnRho_ct&
                  -(E/T)*dlnPdlnT_cd*d2lnEdlnRhodlnT/dlnPdlnRho_ct&
                  +(E/T)*dlnPdlnT_cd*dlnEdlnRho_ct/dlnPdlnRho_ct&
                  +(E/T)*dlnPdlnT_cd*dlnEdlnRho_ct*d2lnPdlnRhodlnT/dlnPdlnRho_ct**2&
                  -(P/(d*T))*dlnPdlnT_cd**2&
                  -(P/(d*T))*d2lnPdlnT2_cd&
                  +(P/(d*T))*dlnPdlnT_cd&
                  +(P/(d*T))*dlnPdlnT_cd**2/dlnPdlnRho_ct &
                  +(P/(d*T))*d2lnPdlnT2_cd/dlnPdlnRho_ct&
                  -(P/(d*T))*dlnPdlnT_cd*d2lnPdlnRhodlnT/dlnPdlnRho_ct**2&
                  -(P/(d*T))*dlnPdlnT_cd/dlnPdlnRho_ct&
                  +(E/T)*dlnEdlnT_cd**2&
                  +(E/T)*d2lnEdlnT2_cd&
                  -(E/T)*dlnEdlnT_cd&
                  +(P/(d*T))*dlnPdlnT_cd**2 &
                  +(P/(d*T))*d2lnPdlnT2_cd&
                  -(P/(d*T))*dlnPdlnT_cd
    res(i_lnPgas) = lnP
    d_dlnRho_c_T(i_lnPgas) = dlnPdlnRho_ct
    d_dlnT_c_Rho(i_lnPgas) = dlnPdlnT_cd
    res(i_lnE) = lnE
    d_dlnRho_c_T(i_lnE) = dlnEdlnRho_ct
    d_dlnT_c_Rho(i_lnE) = dlnEdlnT_cd
    res(i_lnS) = lnS
    d_dlnRho_c_T(i_lnS) = dlnSdlnRho_ct
    d_dlnT_c_Rho(i_lnS) = dlnSdlnT_cd

    res(i_Cp) = Cp
    d_dlnT_c_Rho(i_Cp) = dCpdlnT_cd
    d_dlnRho_c_T(i_Cp) = dCpdlnRho_ct

    res(i_grad_ad) = 1./(-dlnSdlnT_cd*dlnPdlnRho_ct/dlnSdlnRho_ct + dlnPdlnT_cd)
    d_dlnRho_c_T(i_grad_ad) = (- res(i_grad_ad)**2)&
                                *(-d2lnSdlnRhodlnT*dlnPdlnRho_ct/dlnSdlnRho_ct &
                                  -dlnSdlnT_cd*d2lnPdlnRho2_ct/dlnSdlnRho_ct&
                                  +dlnSdlnT_cd*d2lnSdlnRho2_ct*dlnPdlnRho_ct/dlnSdlnRho_ct**2&
                                  + d2lnPdlnRhodlnT)    
    d_dlnT_c_Rho(i_grad_ad) = (- res(i_grad_ad)**2) &
                              *(-d2lnSdlnT2_cd*dlnPdlnRho_ct/dlnSdlnRho_ct &
                                - dlnSdlnT_cd*d2lnPdlnRhodlnT/dlnSdlnRho_ct &
                                +dlnSdlnT_cd * d2lnSdlnRhodlnT*dlnPdlnRho_ct/dlnSdlnRho_ct**2&
                                +d2lnPdlnT2_cd)

    res(i_chiRho) = dlnPdlnRho_ct
    d_dlnRho_c_T(i_chiRho) = d2lnPdlnRho2_ct
    d_dlnT_c_Rho(i_chiRho) = d2lnPdlnRhodlnT
      
    res(i_chiT) = dlnPdlnT_cd
    d_dlnRho_c_T(i_chiT) = d2lnPdlnRhodlnT
    d_dlnT_c_Rho(i_chiT) = d2lnPdlnT2_cd
      
    res(i_Cv) = (E/T)*dlnEdlnT_cd
    d_dlnRho_c_T(i_Cv) = (E / T) * dlnEdlnRho_ct * dlnEdlnT_cd &
         + (E / T) * d2lnEdlnRhodlnT
    d_dlnT_c_Rho(i_Cv) = (E / T) * dlnEdlnT_cd**2 &
         - (E / T) * dlnEdlnT_cd + (E/T) * d2lnEdlnT2_cd
    
    res(i_dE_dRho) = dlnEdlnRho_ct * E / d
    d_dlnRho_c_T(i_dE_dRho) = (E / d) * dlnEdlnRho_ct**2 - (E / d)*dlnEdlnRho_ct &
         + (E/d)*d2lnEdlnRho2_ct
    d_dlnT_c_Rho(i_dE_dRho) = (E/d) * dlnEdlnT_cd * dlnEdlnRho_ct &
         + (E/d)*d2lnEdlnRhodlnT

    res(i_dS_dT) = dlnSdlnT_cd * S / T
    d_dlnRho_c_T(i_dS_dt) = (S / T) * dlnSdlnRho_ct * dlnSdlnT_cd &
         + (S / T) * d2lnSdlnRhodlnT  !! Equation Checked
    d_dlnT_c_Rho(i_dS_dt) = (S / T) * dlnSdlnT_cd**2d0 &
         - (S / T) * dlnSdlnT_cd + (S/T) * d2lnSdlnT2_cd  !! Equation Checked

    res(i_dS_dRho) = dlnSdlnRho_ct * S / d
    d_dlnRho_c_T(i_dS_dRho) = (S / d) * dlnSdlnRho_ct**2 - (S / d)*dlnSdlnRho_ct &
         + (S/d) * d2lnSdlnRho2_ct  !! Equation Checked
    d_dlnT_c_Rho(i_dS_dRho) = (S / d) * dlnSdlnT_cd * dlnSdlnRho_ct &
         + (S / d) * d2lnSdlnRhodlnT  !! Equation Checked

      !! This may be a function of Rho and T
!      res(i_mu) = aneos_species_tbl(index)%mu  
!      d_dlnT_c_Rho(i_mu) = 0.
!      d_dlnRho_c_T(i_mu) = 0.

!    res(i_lnfree_e) = 1d-20
!    d_dlnT_c_Rho(i_lnfree_e) = 0.
!    d_dlnRho_c_T(i_lnfree_e) = 0.

    res(i_gamma1) = dlnPdlnRho_ct-dlnSdlnRho_ct*dlnPdlnT_cd/dlnSdlnT_cd
    d_dlnRho_c_T(i_gamma1) = d2lnPdlnRho2_ct-d2lnSdlnRho2_ct*dlnPdlnT_cd/dlnSdlnT_cd&
         -dlnSdlnRho_ct*d2lnPdlnRhodlnT/dlnSdlnT_cd &
         +dlnSdlnRho_ct*d2lnSdlnRhodlnT*dlnPdlnT_cd/dlnSdlnT_cd**2
    d_dlnT_c_Rho(i_gamma1) = d2lnPdlnRhodlnT &
         -d2lnSdlnRhodlnT*dlnPdlnT_cd/dlnSdlnT_cd&
         -dlnSdlnRho_ct*d2lnPdlnT2_cd/dlnSdlnT_cd&
         +dlnSdlnRho_ct*d2lnSdlnT2_cd*dlnPdlnT_cd/dlnSdlnT_cd**2    !! Page 87
    res(i_gamma3) = 1 - dlnSdlnRho_ct/dlnSdlnT_cd
    d_dlnT_c_Rho(i_gamma3) = -d2lnSdlnRho2_cT/dlnSdlnT_cd &
         + dlnSdlnRho_ct*d2lnSdlnRhodlnT/dlnSdlnT_cd**2
    d_dlnRho_c_T(i_gamma3) = -d2lnSdlnRhodlnT/dlnSdlnT_cd &
         +dlnSdlnRho_ct*d2lnSdlnT2_cd/dlnSdlnT_cd**2  !! Derived on page 87 of notebook
    
!      res(i_eta) = 0.
!      d_dlnT_c_Rho(i_eta) = 0.
!      d_dlnRho_c_T(i_eta) = 0.
    
  end subroutine convlnderivs_RES


  subroutine convRhoMatVolMat(RhoMat_in, VolMat_out, num_spec)
    use watereos_def
    real(dp), dimension(:,:), intent(in) :: RhoMat_in
    real(dp), dimension(:,:), intent(out) :: VolMat_out
    integer, intent(in) :: num_spec

    VolMat_out(1:num_spec,i_val) = 1d0 / RhoMat_in(1:num_spec,i_val)
    VolMat_out(1:num_spec,i_dP) = -1d0 * VolMat_out(1:num_spec,i_val)**2d0 * RhoMat_in(1:num_spec,i_dP)
    VolMat_out(1:num_spec,i_dT) = -1d0 * VolMat_out(1:num_spec,i_val)**2d0 * RhoMat_in(1:num_spec,i_dT)
    VolMat_out(1:num_spec,i_dP2) = -2d0 * VolMat_out(1:num_spec,i_val) * VolMat_out(1:num_spec,i_dP)**2d0 &
         - VolMat_out(1:num_spec,i_val)**2d0 * RhoMat_in(1:num_spec, i_dP2)
    VolMat_out(1:num_spec,i_dT2) = -2d0 * VolMat_out(1:num_spec,i_val) * VolMat_out(1:num_spec,i_dT)**2d0 &
         - VolMat_out(1:num_spec,i_val)**2d0 * RhoMat_in(1:num_spec, i_dT2)
    VolMat_out(1:num_spec,i_dPdT) = -2d0 * VolMat_out(1:num_spec,i_val) &
         * VolMat_out(1:num_spec,i_dP) * VolMat_out(1:num_spec,i_dT) &
         - VolMat_out(1:num_spec,i_val)**2d0 * RhoMat_in(1:num_spec, i_dPdT)

  end subroutine convRhoMatVolMat



  subroutine convVolVecRhoVec(VolVec_in, RhoVec_out)
    use watereos_def
    real(dp), dimension(:), intent(in) :: VolVec_in
    real(dp), dimension(:), intent(out) :: RhoVec_out

    RhoVec_out(i_val) = 1d0 / VolVec_in(i_val)
    RhoVec_out(i_dP) = -1d0 * RhoVec_out(i_val)**2d0 * VolVec_in(i_dP)
    RhoVec_out(i_dT) = -1d0 * RhoVec_out(i_val)**2d0 * VolVec_in(i_dT)
    RhoVec_out(i_dP2) = -2d0 * RhoVec_out(i_val) * VolVec_in(i_dP)**2d0 &
         - RhoVec_out(i_val)**2d0 * VolVec_in(i_dP2)
    RhoVec_out(i_dT2) = -2d0 * RhoVec_out(i_val) * VolVec_in(i_dT)**2d0 &
         - RhoVec_out(i_val)**2d0 * VolVec_in(i_dT2)
    RhoVec_out(i_dPdT) = -2d0 * RhoVec_out(i_val) * VolVec_in(i_dP) * VolVec_in(i_dT) &
         - RhoVec_out(i_val)**2d0 * VolVec_in(i_dPdT)
    
  end subroutine convVolVecRhoVec

  subroutine AddVolMix(VolMat_in, SMat_in, EMat_in, X_i, num_spec, &
       VolVect_out, EVect_out, SVect_out)
    use watereos_def

    real(dp), dimension(num_spec,num_derivs), intent(in) :: VolMat_in, SMat_in, EMat_in
    real(dp), dimension(num_spec), intent(in) :: X_i
    integer, intent(in) :: num_spec
    real(dp), intent(out) :: VolVect_out(num_derivs), &
         EVect_out(num_derivs), SVect_out(num_derivs)
    integer :: i,j
    
    VolVect_out(:) = 0d0
    SVect_out(:) = 0d0
    EVect_out(:) = 0d0
    
    if(mixdbg) then
       write(*,*) "[AddVolMix/num_spec]", num_spec
    endif
    do i=1,num_spec       
       if(mixdbg) then
          write(*,*), X_i(i)
       endif
       if(X_i(i) .gt. 0d0) then
          do j=1,num_derivs
             VolVect_out(j) = VolVect_out(j) + X_i(i)*VolMat_in(i,j)
             EVect_out(j) = EVect_out(j) + X_i(i)*EMat_in(i,j)
             SVect_out(j) = SVect_out(j) + X_i(i)*SMat_in(i,j)
          enddo
       endif
    enddo

  end subroutine AddVolMix



  !! Check that these two routines are in fact inverse functions of each other
  
  !! Converts from the format [F, dFdX, dFdY, d2FdX2, d2FdY2, d2FdXdY] to 
  !!                      to  [lnF, dlnFdlnX, dlnFdlnY, d2lnFdlnX2, d2lnFdlnY2, d2lnFdlnXdlnY]
  subroutine convDerivslnDerivs(Rho, T, Pvect, Evect, Svect, lnPvect, lnEvect, lnSvect)
    use watereos_def

    real(dp), intent(in) :: Rho, T
    real(dp), intent(in) :: Pvect(6), Evect(6), Svect(6)
    real(dp), intent(out) :: lnPvect(6), lnEvect(6), lnSvect(6)

    call conv_vector(Pvect, lnPvect)
    call conv_vector(Evect, lnEvect)
    call conv_vector(Svect, lnSvect)
    
    contains

      subroutine conv_vector(Fvect, lnFvect)
        real(dp), intent(in) :: Fvect(6)
        real(dp), intent(out) :: lnFvect(6)

        lnFvect(i_val) = log(Fvect(i_val))
        lnFvect(i_dX) = (Rho / Fvect(i_val)) * Fvect(i_dX)
        lnFvect(i_dY) = (T / Fvect(i_val)) * Fvect(i_dY)
        lnFvect(i_dX2) = lnFvect(i_dX) - lnFvect(i_dX)**2 + (Rho**2 / Fvect(i_val)) * Fvect(i_dX2)
        lnFvect(i_dY2) = lnFvect(i_dY) - lnFvect(i_dY)**2 + (T**2 / Fvect(i_val)) * Fvect(i_dY2)
        lnFvect(i_dXdY) = - lnFvect(i_dX) * lnFvect(i_dY) + (Rho * T / Fvect(i_val)) * Fvect(i_dXdY)
      end subroutine conv_vector

  end subroutine convDerivslnDerivs


  !! Converts from the format [lnF, dlnFdlnX, dlnFdlnY, d2lnFdlnX2, d2lnFdlnY2, d2lnFdlnXdlnY]
  !!  to                      [F, dFdX, dFdY, d2FdX2, d2FdY2, d2FdXdY] to 

  subroutine convlnDerivsDerivs(lnRho, lnT, lnPvect, lnEvect, lnSvect, Pvect, Evect, Svect)
    use watereos_def

    real(dp), intent(in) :: lnRho, lnT
    real(dp), intent(in) :: lnPvect(6), lnEvect(6), lnSvect(6)
    real(dp), intent(out) :: Pvect(6), Evect(6), Svect(6)
    
    real(dp) :: Rho, T

    Rho = exp(lnRho)
    T = exp(lnT)
    
    call conv_vector(lnPvect, Pvect)
    call conv_vector(lnEvect, Evect)
    call conv_vector(lnSvect, Svect)
    
  contains
    subroutine conv_vector(lnFvect, Fvect)
      real(dp), intent(in) :: lnFvect(6)
      real(dp), intent(out) :: Fvect(6)
      Fvect(i_val) = exp(lnFvect(i_val))
      Fvect(i_dX) = (Fvect(i_val) / Rho) * lnFvect(i_dX)
      Fvect(i_dY) = (Fvect(i_val) / T) * lnFvect(i_dY)
      Fvect(i_dX2) = (Fvect(i_val) / Rho**2 ) *lnFvect(i_dX2) &
                     - (1. / Rho) * Fvect(i_dX) &
                     + (1. / Fvect(i_val)) * (Fvect(i_dX)**2)
      Fvect(i_dY2) = (Fvect(i_val) / T**2) * lnFvect(i_dY2) &
                     - (1. / T) * Fvect(i_dY) &
                     + (1. / Fvect(i_val)) * (Fvect(i_dY)**2)
      Fvect(i_dXdY) = (Fvect(i_val) / (Rho * T)) * &
                        (lnFvect(i_dXdY) + lnFvect(i_dX) * lnFvect(i_dY))
    end subroutine conv_vector
  end subroutine convlnDerivsDerivs
  
  subroutine convDF_thermo(rho, t, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
       d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, &
       Pvect, Evect, Svect)
    use watereos_def    
    real(dp), intent(in) :: rho, t, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
         d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3
    real(dp), dimension(:), pointer, intent(out) :: Pvect, Evect, Svect
    real(dp) :: RhoSQ
    
    RhoSQ = rho*rho
    Pvect(i_val) = RhoSQ * df_drho
    Pvect(i_drho) = RhoSQ * d2f_drho2 + 2 * rho * df_drho
    Pvect(i_dt) = RhoSQ * d2f_drhodt
    Pvect(i_drho2) = RhoSQ * d3f_drho3 + 4. * rho * d2f_drho2 + 2. * df_drho 
    Pvect(i_dt2) = RhoSQ * d3f_drhodt2
    Pvect(i_drhodt) = RhoSQ * d3f_drho2dt + 2. * rho * d2f_drhodt
    
    Svect(i_val) = -df_dt
    Svect(i_drho) = -d2f_drhodt
    Svect(i_dt) = -d2f_dt2
    Svect(i_drho2) = -d3f_drho2dt
    Svect(i_dt2) = - d3f_dt3
    Svect(i_drhodt) = - d3f_drhodt2
    
    Evect(i_val) = f_val - T * df_dt
    Evect(i_drho) = df_drho - T * d2f_drhodt
    Evect(i_dt) = -T * d2f_dt2
    Evect(i_drho2) = d2f_drho2 - T*d3f_drho2dt
    Evect(i_dt2) = -d2f_dt2 - T * d3f_dt3
    Evect(i_drhodt) = - T * d3f_drhodt2
  end subroutine convDF_thermo
  

end module thermo
