module ideal_water
  use watereos_def
  implicit none

  !! Simple mathematical identity - scratch
  !! log(X,A) = log(X,B) / log(A,B)
  !! => log(X,B) = log(X,A) * log(A,B)
  
  !! These are an extreme range which is totally beyond what is physical
  real(dp), parameter :: minlogRho = -10.
  real(dp), parameter :: maxlogRho = 3.
  real(dp), parameter :: minlogT = 1.5
  real(dp), parameter :: maxlogT = 10.

  integer, parameter :: n_iparams = 3
  integer, parameter :: i_which_other = 1
  integer, parameter :: i_eos_calls = 2
  integer, parameter :: i_rho_flag = 3
  integer, parameter :: i_P_flag = 3

  integer, parameter :: n_rparams = 2
  integer, parameter :: i_other = 1
  integer, parameter :: i_fixed_log_input = 2

  real(dp), parameter :: h_pc = 6.6e-27 
  real(dp), parameter :: k_pc = 1.38e-16 !! erg K^-1
  real(dp), parameter :: mu = 18. !! Water
  real(dp), parameter :: N_A = 6.e23 !! Number of particles per mole
  real(dp), parameter :: m = mu / N_A !! g / particle
  real(dp), parameter :: alpha = (h_pc / (2 * 3.14 * m * k_pc)**0.5)**3.
  real(dp), parameter :: lnN_A_mu = log(N_A / mu)
  real(dp), parameter :: lnalpha = log(alpha)

  real(dp), parameter :: EPSILON = 1d-3
  private convDF_thermo
contains

  !! If you were going to modify this equation of state, then
  !! this is the main routine where you would want to do it.
  !! Ideally you have a simple analytic formula for the helmholtz free energy
  !! which you then can differentiate to get all of the derivatives below
  !! if not, then I'm not sure what we should do

  subroutine iw_get_free(Rho, logRho, T, logT, &
       f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
       d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2)
    implicit none
    real(dp), intent(in) :: Rho, logRho, T, logT
    real(dp), intent(out) :: f_val, df_drho, df_dt, &
         d2f_drho2, d2f_dt2, d2f_drhodt, &
         d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2
!    real(dp) :: N_A, mu, alpha, k, h, m, 
    real(dp) :: lnRho, lnT, rho2, rho3, T2
!lnN_A_mu, lnalpha, rho2, rho3, T2
    
!    h = 6.6e-27 
!    k = 1.38e-16 !! erg K^-1
!    mu = 18. !! Water
!    N_A = 6.e23 !! Number of particles per mole
!    m = 18. / N_A !! g / particle
!    alpha = (h / (2 * 3.14 * m * k)**0.5)**3.
    
    !! I added these so that the logs are only computed once
    lnRho = log(rho)
    lnT = log(T)
    
    !! I added these so that powers aren't calculated more than once
    rho2 = rho*rho
    rho3 = rho2 * rho    
    T2 = T*T
    
    !! Get the equation of state here
    f_val = -(N_A / mu) * k_pc * T * ( - lnRho - lnN_A_mu - lnalpha + 1.5 * lnT + 1.)
    df_drho =  (N_A / mu) * k_pc * T / rho
    df_dt = -(N_A / mu) * k_pc * ( - lnRho - lnN_A_mu - lnalpha + 1.5 * lnT + 2.5)
    d2f_drho2 = -(N_A / mu) * k_pc * T / rho2
    d2f_dt2 =  -(N_A / mu) * 1.5 * k_pc  / T
    d2f_drhodt =  (N_A / mu) * k_pc / rho
    d3f_drho3 = (N_A / mu) * 2. * k_pc * T / rho3
    d3f_drho2dt = -(N_A / mu) * k_pc / rho2
    d3f_drhodt2 = 0.
    d3f_dt3 = (N_A / mu) * 1.5 * k_pc / T2
    d4f_drho2dt2 = 0.
    
  end subroutine iw_get_free
  
  subroutine convDF_thermo(rho, t, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
       d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, &
       Pvect, Evect, Svect)
    
    real(dp), intent(in) :: rho, t, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
         d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3
    real(dp), dimension(:), pointer, intent(out) :: Pvect, Evect, Svect
    real(dp) :: RhoSQ
    
    RhoSQ = rho*rho
    Pvect(i_val) = RhoSQ * df_drho
    Pvect(i_drho) = RhoSQ * d2f_drho2 + 2 * rho * df_drho
    Pvect(i_dt) = RhoSQ * d2f_drhodt
    Pvect(i_drho2) = RhoSQ * d3f_drho3 + 4. * rho * d2f_drho2 + 2. * df_drho 
    Pvect(i_dt2) = RhoSQ * d3f_drhodt2
    Pvect(i_drhodt) = RhoSQ * d3f_drho2dt + 2. * rho * d2f_drhodt
    
    Svect(i_val) = -df_dt
    Svect(i_drho) = -d2f_dt2
    Svect(i_dt) = -d2f_drhodt
    Svect(i_drho2) = -d3f_drho2dt
    Svect(i_dt2) = - d3f_dt3
    Svect(i_drhodt) = - d3f_drhodt2
    
    Evect(i_val) = f_val - T * df_dt
    Evect(i_drho) = df_drho - T * d2f_drhodt
    Evect(i_dt) = -T * d2f_dt2
    Evect(i_drho2) = d2f_drho2 - T*d3f_drho2dt
    Evect(i_dt2) = -d2f_dt2 - T * d3f_dt3
    Evect(i_drhodt) = - T * d3f_drhodt2
  end subroutine convDF_thermo
  
  subroutine iw_get_val(Rho, logRho, T, logT, Pvect, Evect, Svect, ierr)
    real(dp), intent(in) :: Rho, logRho, T, logT
    real(dp), intent(out), pointer, dimension(:) :: Pvect, Evect, Svect
    integer, intent(out) :: ierr

    real(dp) :: f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
         d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2
    
    call iw_get_free(Rho, logRho, T, logT, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
         d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2)
    
    call convDF_thermo(Rho, T, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
       d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, &
       Pvect, Evect, Svect)    

    if(Svect(i_val) .lt. 0) then
       write(*,*) "iw_get_val  dbg"
       write(*,*) "df_dt: ", df_dt
    endif
  end subroutine iw_get_val

  subroutine iw_get_res(Rho, logRho, T, logT, res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
    use watereos_def
    use thermo
    use eos_def
    
    real(dp), intent(in) :: Rho, logRho, T, logT
    real(dp), intent(out) :: res(num_eos_basic_results), &
         d_dlnRho_c_T(num_eos_basic_results), d_dlnT_c_Rho(num_eos_basic_results)
    integer, intent(out) :: ierr

    real(dp), pointer, dimension(:) :: Pvect, Evect, Svect, &
         lnPvect, lnEvect, lnSvect
    real(dp) :: lnRho, lnT

    ierr = 0
    res = 0
    d_dlnRho_c_T = 0
    d_dlnT_c_Rho = 0
    
    lnRho = log(Rho)
    lnT   = log(T)
    
    allocate(Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs), &
         lnPvect(num_derivs), lnEvect(num_derivs), lnSvect(num_derivs))
    
    call iw_get_val(Rho, logRho, T, logT, Pvect, Evect, Svect, ierr)    
    call convDerivslnDerivs(Rho, T, Pvect, Evect, Svect, lnPvect, lnEvect, lnSvect)
    call convlnderivs_RES(lnRho, lnT, lnPvect, lnEvect, lnSvect, &
         res, d_dlnRho_c_T, d_dlnT_c_Rho)
    
    if(isnan(res(i_lnS))) then
       write(*,*) "iw_get_res dbg"
       write(*,*) "Rho : ", Rho
       write(*,*) "T   : ", T
       write(*,*) "Svect: ", Svect
    endif

    deallocate(Pvect, Evect, Svect)
    deallocate(lnPvect, lnEvect, lnSvect)
  end subroutine iw_get_res
    
  subroutine iwRho_PT(P, T, Rho)
    real(dp), intent(in) :: P, T
    real(dp), intent(out) :: Rho
    
    real(dp) :: logRho

    Rho = P * (mu / N_A) / (k_pc * T)
    logRho = log10(Rho)
    if(logRho .gt. maxlogRho) then 
       logRho = maxlogRho
       Rho = 1d1**logRho
    endif
  end subroutine iwRho_PT

  !! For the ideal gas law, this is extremely trivial since we can use the analytic relationship
  !!  between Pressure and Density to perform this inversion.  More typically, we will be 
  !!  doing a rootfind
  subroutine iwPT_get_res(P, logP, T, logT, Rho, logRho, res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
    use watereos_def
    use thermo
    use eos_def
    use const_def, only: ln10

    real(dp), intent(in) :: P, logP, T, logT
    real(dp), intent(out) :: Rho, logRho
    real(dp), intent(out) :: res(num_eos_basic_results), d_dlnRho_c_T(num_eos_basic_results), &
         d_dlnT_c_Rho(num_eos_basic_results)
    integer, intent(out) :: ierr

    real(dp) :: tightMaxlogRho
    
    Rho = P * (mu / N_A) / (k_pc * T)
    logRho = log10(Rho)
    
    tightMaxlogRho = min(maxlogRho, (2.5 - lnN_A_mu - lnalpha + 1.5 * log(T)) / ln10)
    

    if((logRho .gt. tightMaxlogRho) .or. (logRho .lt. minlogRho)) then
       write(*,*) "You are calling the function out of the density range!"
       ierr = IW_ERROR_OUTOFRANGE
    else
       call iw_get_res(Rho, logRho, T, logT, res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
    endif
  end subroutine iwPT_get_res


  subroutine iw_get_t(logRho, which_other, other_value, logT_tol, other_tol, max_iter, logT_guess, &
       logT_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    use eos_def
    use const_def, only: ln10

    real(dp), intent(in) :: logRho
    integer, intent(in) :: which_other
    real(dp), intent(in) :: other_value
    real(dp), intent(in) :: logT_tol, other_tol
    integer, intent(in) :: max_iter
    real(dp), intent(in) :: logT_guess
    
    real(dp), intent(out) :: logT_result
    real(dp), intent(out) :: res(num_eos_basic_results), d_dlnRho_c_T(num_eos_basic_results), &
         d_dlnT_c_Rho(num_eos_basic_results)
    integer, intent(out) :: eos_calls, ierr
    
    logical, parameter :: doing_rho = .false.
    real(dp) :: guess_logT, tightMinlogT
    real(dp) :: other_at_minlogT, other_at_maxlogT
    real(dp) :: tmp_res(num_eos_basic_results), tmp_d_dlnRho_c_T(num_eos_basic_results), &
         tmp_d_dlnT_c_Rho(num_eos_basic_results)
    real(dp) :: tmp_Rho, tmp_lnRho, tmp_T, tmp_lnT
    
    tmp_Rho = 1d1**logRho
    tmp_lnRho = logRho * ln10

    tightMinlogT = max(minlogT, ((logRho * ln10 - 2.5 + lnN_A_mu + lnalpha) / 1.5d0) / ln10) + EPSILON
    tmp_T = 1d1**tightMinlogT
    tmp_lnT = tightMinlogT * ln10
    call iw_get_res(tmp_Rho, tmp_lnRho, tmp_T, tmp_lnT, &
         tmp_res, tmp_d_dlnRho_c_T, tmp_d_dlnT_c_Rho, ierr) !! In principle, this shouldn't ever fail
    other_at_minlogT = tmp_res(which_other)
    
    tmp_T = 1d1**maxlogT
    tmp_lnT = maxlogT * ln10
    call iw_get_res(tmp_Rho, tmp_lnRho, tmp_T, tmp_lnT, &
         tmp_res, tmp_d_dlnRho_c_T, tmp_d_dlnT_c_Rho, ierr) 
    other_at_maxlogT = tmp_res(which_other)
    
    guess_logT = (minlogT+maxlogT) / 2d0
    

    if((other_value - other_at_maxlogT) * (other_value - other_at_minlogT) .gt. 0d0) then
       ierr = IW_ERROR_OUTOFRANGE
       write(*,*) "Other value out fo range - target other: " , other_value
       write(*,*) other_at_minlogT, other_at_maxlogT
       return
    else
       call iw_do_safe_get_Rho_T(logRho, which_other, other_value, doing_rho, &
            guess_logT, logT_result, &
            minlogT, maxlogT, other_at_minlogT, other_at_maxlogT, &
            logT_tol, other_tol, max_iter, &
            res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    endif
  end subroutine iw_get_t
  
  subroutine iw_get_rho(logT, which_other, other_value, &
       logRho_tol, other_tol, max_iter, logRho_guess, &
       logRho_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    use eos_def
    use const_def, only: ln10

    real(dp), intent(in) :: logT
    integer, intent(in) :: which_other
    real(dp), intent(in) :: other_value
    real(dp), intent(in) :: logRho_tol, other_tol
    integer, intent(in) :: max_iter
    real(dp), intent(in) :: logRho_guess
    
    real(dp), intent(out) :: logRho_result
    real(dp), intent(out) :: res(num_eos_basic_results), &
         d_dlnRho_c_T(num_eos_basic_results), &
         d_dlnT_c_Rho(num_eos_basic_results)
    integer, intent(out) :: eos_calls, ierr
    
    logical, parameter :: doing_rho = .true.
    real(dp) :: guess_logRho, tightMaxlogRho
    real(dp) :: other_at_minlogRho, other_at_maxlogRho
    real(dp) :: tmp_res(num_eos_basic_results), &
         tmp_d_dlnRho_c_T(num_eos_basic_results), &
         tmp_d_dlnT_c_Rho(num_eos_basic_results)
    real(dp) :: tmp_Rho, tmp_lnRho, tmp_T, tmp_lnT
    
    tmp_Rho = 1d1**minlogRho
    tmp_lnRho = minlogRho * ln10
    tmp_T = 1d1**logT
    tmp_lnT = logT * ln10
    call iw_get_res(tmp_Rho, tmp_lnRho, tmp_T, tmp_lnT, &
         tmp_res, tmp_d_dlnRho_c_T, tmp_d_dlnT_c_Rho, ierr) !! In principle, this shouldn't ever fail
    other_at_minlogRho = tmp_res(which_other)
    

    tightMaxlogRho = min(maxlogRho, (2.5 - lnN_A_mu - lnalpha + 1.5 * logT * ln10) / ln10) - EPSILON
    tmp_Rho = 1d1**tightMaxlogRho
    tmp_lnRho = tightMaxlogRho * ln10
    call iw_get_res(tmp_Rho, tmp_lnRho, tmp_T, tmp_lnT, &
         tmp_res, tmp_d_dlnRho_c_T, tmp_d_dlnT_c_Rho, ierr) 
    other_at_maxlogRho = tmp_res(which_other)
    
    if((other_value - other_at_maxlogRho) * (other_value - other_at_minlogRho) .gt. 0d0) then !! Don't do the lookup if the value we are looking for isn't between the extrema
       ierr = IW_ERROR_OUTOFRANGE
       return
    else
       call iw_do_safe_get_Rho_T(logT, which_other, other_value, doing_rho, &
            guess_logRho, logRho_result, &
            minlogRho, maxlogRho, other_at_minlogRho, other_at_maxlogRho, &
            logRho_tol, other_tol, max_iter, &
            res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    end if
  end subroutine iw_get_rho
  
  subroutine iwPT_get_P(logT, which_other, other_value, &
       logP_tol, other_tol, max_iter, &
       logP_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    use eos_def
    use const_def, only: ln10
    
    real(dp), intent(in) :: logT
    integer, intent(in) :: which_other
    real(dp), intent(in) :: other_value
    real(dp), intent(in) :: logP_tol, other_tol
    integer, intent(in) :: max_iter
    real(dp), intent(out) :: logP_result
    real(dp), intent(out) :: res(num_eos_basic_results), &
         d_dlnRho_c_T(num_eos_basic_results), &
         d_dlnT_c_Rho(num_eos_basic_results)
    integer, intent(out) :: eos_calls, ierr
    
    real(dp) :: logP_guess, minlogP, maxlogP, &
         other_at_minlogP, other_at_maxlogP, &
         tmp_Rho, tmp_lnRho, tmp_T, tmp_lnT, tightMaxlogRho
    real(dp) :: tmp_res(num_eos_basic_results), &
         tmp_d_dlnRho_c_T(num_eos_basic_results), &
         tmp_d_dlnT_c_Rho(num_eos_basic_results)
    logical :: doing_P = .true.

    tmp_Rho = 1d1**minlogRho
    tmp_lnRho = minlogRho * ln10
    tmp_T = 1d1**logT
    tmp_lnT = logT * ln10
    call iw_get_res(tmp_Rho, tmp_lnRho, tmp_T, tmp_lnT, &
         tmp_res, tmp_d_dlnRho_c_T, tmp_d_dlnT_c_Rho, ierr)
    other_at_minlogP = tmp_res(which_other)
    minlogP = tmp_res(i_lnPgas) / ln10
    
    tightMaxlogRho = min(maxlogRho, (2.5 - lnN_A_mu - lnalpha + 1.5 * logT *ln10) / ln10) - EPSILON
    tmp_Rho   = 1d1**tightMaxlogRho
    tmp_lnRho = tightMaxlogRho * ln10
    call iw_get_res(tmp_Rho, tmp_lnRho, tmp_T, tmp_lnT, &
         tmp_res, tmp_d_dlnRho_c_T, tmp_d_DlnT_c_Rho, ierr)
    other_at_maxlogP = tmp_res(which_other)
    maxlogP = tmp_res(i_lnPgas) / ln10

    logP_guess = (minlogP + maxlogP) / 2d0

    if((other_value - other_at_minlogP) * (other_value - other_at_maxlogP) .gt. 0d0) then
       ierr = IW_ERROR_OUTOFRANGE
       return
    else
       call iwPT_do_safe_get_P_T(logT, which_other, other_value, doing_P, &
            logP_guess, logP_result, &
            minlogP, maxlogP, other_at_minlogP, other_at_maxlogP, &
            logP_tol, other_tol, max_iter, &
            res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    end if
    
  end subroutine iwPT_get_P

  subroutine iwPT_get_T(logP, which_other, other_value, logT_tol, other_tol, max_iter,  &
       logT_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    use eos_def
    use const_def, only: ln10

    real(dp), intent(in) :: logP
    integer, intent(in) :: which_other
    real(dp), intent(in) :: other_value
    real(dp), intent(in) :: logT_tol, other_tol
    integer, intent(in) :: max_iter
    
    real(dp), intent(out) :: logT_result
    real(dp), intent(out) :: res(num_eos_basic_results), d_dlnRho_c_T(num_eos_basic_results), &
         d_dlnT_c_Rho(num_eos_basic_results)
    integer, intent(out) :: eos_calls, ierr
    
    real(dp) :: logT_guess, &
         other_at_minlogT, other_at_maxlogT, &
         tmp_Rho, tmp_lnRho, tmp_T, tmp_lnT
    real(dp) :: tmp_res(num_eos_basic_results), &
         tmp_d_dlnRho_c_T(num_eos_basic_results), &
         tmp_d_dlnT_c_Rho(num_eos_basic_results)

    real(dp) :: P, MinRho, MaxRho
    real(dp) :: minT_vert, maxT_vert, minlogT_vert, maxlogT_vert, &
         minlogT_use, maxlogT_use, T, logT, Rho, logRho
    logical :: doing_P = .false.

    P = 1d1**logP    
    !! Minimum T that we can go to along the Minimum Density line
    MaxRho = 1d1**maxlogRho
    minT_vert = P / ((k_pc * N_A/mu) * MaxRho)
    
    minlogT_vert = log10(minT_vert)
    minlogT_use = max(minlogT, minlogT_vert) 
    minlogT_use = max(minlogT_use, ((logP*ln10 - 2.5 - log(k_pc) + lnalpha) / 2.5 / ln10)) + EPSILON
!    write(*,*) "minlogT: ", minlogT, minlogT_vert, minlogT_use
    
    !! Maximum T that we can go to along the Maximum Density line
    MinRho = 1d1**minlogRho
    maxT_vert = P / ((k_pc * N_A / mu) * MinRho)
    maxlogT_vert = log10(maxT_vert)
    maxlogT_use = min(maxlogT, maxlogT_vert) 
    
!    write(*,*) "maxlogT: ", maxlogT, maxlogT_vert, maxlogT_use
    logT_guess = (minlogT_use + maxlogT_use) / 2.

    if(minlogT_use .gt. maxlogT_use) then
       ierr = IW_ERROR_OUTOFRANGE
       write(*,*) "Error T range: ", minlogT_use, maxlogT_use
       return
    else
       T = 1d1**minlogT_use
       logT = minlogT_use
       call iwPT_get_res(P, logP, T, logT, Rho, logRho, res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
       other_at_minlogT = res(which_other)
       if(ierr .ne. 0) then
          write(*,*) "minlogT_use: ", minlogT_use
          write(*,*) "other at minlogT : ", other_at_minlogT
          write(*,*) "min T res: ", res
          write(*,*) "min T ierr: ", ierr
          return
       end if

       T = 1d1**maxlogT_use
       logT = maxlogT_use
       call iwPT_get_res(P, logP, T, logT, Rho, logRho, res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
       other_at_maxlogT = res(which_other)
       if(ierr .ne. 0) then
          write(*,*) "other at maxlogT : ", other_at_maxlogT
          write(*,*) "maxT ierr: ", ierr
          return
       end if
       
!       write(*,*) "Search: ", other_at_minlogT, other_at_maxlogT, other_value

       call iwPT_do_safe_get_P_T(logP, which_other, other_value, doing_P, &
            logT_guess, logT_result, &
            minlogT_use, maxlogT_use, other_at_minlogT, other_at_maxlogT, &
            logT_tol, other_tol, max_iter, &
            res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)

       !! We need to also return the Rho and logRho value
    endif

  end subroutine iwPT_get_T

  !! This routine is called by get_Rho and get_T
  subroutine iw_do_safe_get_Rho_T( &
       the_other_log, & 
       which_other, other_value, doing_Rho, & 
       initial_guess, x, xbnd1, xbnd2, other_at_bnd1, other_at_bnd2, & 
       xacc, yacc, ntry, & 
       res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    use eos_def
    use num_lib
    use const_def
    implicit none
    
    integer, intent(in) :: which_other
    real(dp), intent(in) :: other_value
    logical, intent(in) :: doing_Rho
    real(dp), intent(in) :: initial_guess ! for x
    real(dp), intent(out) :: x ! if doing_Rho, then logRho, else logT
    real(dp), intent(in) :: the_other_log
    real(dp), intent(in) :: xbnd1, xbnd2, other_at_bnd1, other_at_bnd2
    real(dp), intent(in) :: xacc, yacc ! tolerances
    integer, intent(in) :: ntry ! max number of iterations        
    real(dp), intent(out) :: res(num_eos_basic_results)
    real(dp), intent(out) :: d_dlnRho_c_T(num_eos_basic_results)
    real(dp), intent(out) :: d_dlnT_c_Rho(num_eos_basic_results)
    integer, intent(out) :: eos_calls, ierr
    
    integer :: i, j, lrpar, lipar
    integer, pointer :: ipar(:)
    real(dp), pointer :: rpar(:)
    real(dp) :: xb1, xb3, y1, y3, dfdx, f!, rho, t, logRho, logT
    
    ierr = 0    
    eos_calls = 0
    x = initial_guess
    
    lipar = n_iparams 
    lrpar = n_rparams + num_eos_basic_results*3
    allocate(ipar(lipar),rpar(lrpar))

    !! Setup input Integer vector
    if(doing_Rho) then
       ipar(i_rho_flag) = 1
    else
       ipar(i_rho_flag) = 0
    endif
    ipar(i_which_other) = which_other
    ipar(i_eos_calls) = eos_calls   !! set to zero above
 
    !! Setup input real vector
    rpar(i_other) = other_value
    rpar(i_fixed_log_input) = the_other_log
    
    xb1 = xbnd1; xb3 = xbnd2
    y1 = other_at_bnd1 - other_value
    y3 = other_at_bnd2 - other_value
    
    x = safe_root_with_initial_guess(  &
         iw_get_f_df, initial_guess, xb1, xb3, y1, y3, ntry,  &
         xacc, yacc, lrpar, rpar, lipar, ipar, ierr)
    if (ierr /= 0) then
       write(*, *) 'do_safe_get_Rho_T: safe_root returned ierr', ierr
       return
    end if
    
    i = n_rparams
    res = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    d_dlnRho_c_T = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    d_dlnT_c_Rho = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    eos_calls = ipar(i_eos_calls)
    
    deallocate(ipar, rpar)

  end subroutine iw_do_safe_get_Rho_T

  !! This is the function that is passed to safe_root_with_initial_guess
  real(dp) function iw_get_f_df(x, dfdx, lrpar, rpar, lipar, ipar, ierr)
    use eos_def
    use const_def, only: ln10
    implicit none

    integer, intent(in) :: lrpar, lipar
    real(dp), intent(in) :: x
    real(dp), intent(out) :: dfdx
    real(dp), intent(inout), pointer :: rpar(:)
    integer, intent(inout), pointer :: ipar(:)
    integer, intent(out) :: ierr    

    logical :: doing_Rho
    integer :: which_other, i
    real(dp), pointer, dimension(:) :: res, d_dlnRho_c_T, d_dlnT_c_Rho
    real(dp) :: Rho, T, log10Rho, log10T, other, fixed_log_input
    
    ierr = 0
    
    doing_Rho = (ipar(i_rho_flag) /= 0)
    which_other = ipar(i_which_other)
    i = n_iparams + 1
    
    i = 0
    other = rpar(i_other) 
    fixed_log_input = rpar(i_fixed_log_input)
    
    i = n_rparams
    res => rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    d_dlnRho_c_T => rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    d_dlnT_c_Rho => rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    
    if(doing_Rho) then
       log10Rho = x
       Rho = 1d1**log10Rho
       log10T = fixed_log_input
       T = 1d1**log10T !arg_not_provided
    else
       log10T = x
       T = 1d1**log10T
       log10Rho = fixed_log_input
       Rho = 1d1**log10Rho !arg_not_provided
    endif
    
    call iw_get_res( &
         Rho, log10Rho, T, log10T, &
         res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
    if (ierr /= 0) then
       return
    end if
    ipar(i_eos_calls) = ipar(i_eos_calls)+1
    
    iw_get_f_df = res(which_other) - other
    
    if (doing_Rho) then
       dfdx = d_dlnRho_c_T(which_other)*ln10
    else
       dfdx = d_dlnT_c_Rho(which_other)*ln10
    end if
    
  end function iw_get_f_df


  !! This routine is called by get_Rho and get_T
  subroutine iwPT_do_safe_get_P_T( &
       the_other_log, & 
       which_other, other_value, doing_P, & 
       initial_guess, x, xbnd1, xbnd2, other_at_bnd1, other_at_bnd2, & 
       xacc, yacc, ntry, & 
       res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    use eos_def
    use num_lib
    use const_def
    implicit none
    
    integer, intent(in) :: which_other
    real(dp), intent(in) :: other_value
    logical, intent(in) :: doing_P
    real(dp), intent(in) :: initial_guess ! for x
    real(dp), intent(out) :: x ! if doing_Rho, then logRho, else logT
    real(dp), intent(in) :: the_other_log
    real(dp), intent(in) :: xbnd1, xbnd2, other_at_bnd1, other_at_bnd2
    real(dp), intent(in) :: xacc, yacc ! tolerances
    integer, intent(in) :: ntry ! max number of iterations        
    real(dp), intent(out) :: res(num_eos_basic_results)
    real(dp), intent(out) :: d_dlnRho_c_T(num_eos_basic_results)
    real(dp), intent(out) :: d_dlnT_c_Rho(num_eos_basic_results)
    integer, intent(out) :: eos_calls, ierr
    
    integer :: i, j, lrpar, lipar
    integer, pointer :: ipar(:)
    real(dp), pointer :: rpar(:)
    real(dp) :: xb1, xb3, y1, y3, dfdx, f!, rho, t, logRho, logT
    
    ierr = 0    
    eos_calls = 0
    x = initial_guess
    
    lipar = n_iparams 
    lrpar = n_rparams + num_eos_basic_results*3
    allocate(ipar(lipar),rpar(lrpar))

    !! Setup input Integer vector
    if(doing_P) then
       ipar(i_P_flag) = 1
    else
       ipar(i_P_flag) = 0
    endif
    ipar(i_which_other) = which_other
    ipar(i_eos_calls) = eos_calls   !! set to zero above
 
    !! Setup input real vector
    rpar(i_other) = other_value
    rpar(i_fixed_log_input) = the_other_log
    
    xb1 = xbnd1; xb3 = xbnd2
    y1 = other_at_bnd1 - other_value
    y3 = other_at_bnd2 - other_value
    
    x = safe_root_with_initial_guess(  &
         iwPT_get_f_df, initial_guess, xb1, xb3, y1, y3, ntry,  &
         xacc, yacc, lrpar, rpar, lipar, ipar, ierr)
    if (ierr /= 0) then
       write(*, *) 'do_safe_get_Rho_T: safe_root returned ierr', ierr
       return
    end if
    
    i = n_rparams
    res = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    d_dlnRho_c_T = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    d_dlnT_c_Rho = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    eos_calls = ipar(i_eos_calls)
    
    deallocate(ipar, rpar)
    
  end subroutine iwPT_do_safe_get_P_T


  !! This is the function that is passed to safe_root_with_initial_guess
  real(dp) function iwPT_get_f_df(x, dfdx, lrpar, rpar, lipar, ipar, ierr)
    use eos_def
    use const_def, only: ln10
    implicit none

    integer, intent(in) :: lrpar, lipar
    real(dp), intent(in) :: x
    real(dp), intent(out) :: dfdx
    real(dp), intent(inout), pointer :: rpar(:)
    integer, intent(inout), pointer :: ipar(:)
    integer, intent(out) :: ierr    

    logical :: doing_P
    integer :: which_other, i
    real(dp), pointer, dimension(:) :: res, d_dlnRho_c_T, d_dlnT_c_Rho
    real(dp) :: P, T, Rho, log10P, log10T, log10Rho, other, fixed_log_input
    
    ierr = 0
    
    doing_P = (ipar(i_P_flag) /= 0)
    which_other = ipar(i_which_other)
    i = n_iparams + 1
    
    i = 0
    other = rpar(i_other) 
    fixed_log_input = rpar(i_fixed_log_input)
    
    i = n_rparams
    res => rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    d_dlnRho_c_T => rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    d_dlnT_c_Rho => rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    
    if(doing_P) then
       log10P = x
       P = 1d1**log10P
       log10T = fixed_log_input
       T = 1d1**log10T !arg_not_provided
    else
       log10T = x
       T = 1d1**log10T
       log10P = fixed_log_input
       P = 1d1**log10P !arg_not_provided
    endif
    
    call iwPT_get_res( &
         P, log10P, T, log10T, Rho, log10Rho, &
         res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
    if (ierr /= 0) then
       return
    end if
    ipar(i_eos_calls) = ipar(i_eos_calls)+1
    
    iwPT_get_f_df = res(which_other) - other

    
    if (doing_P) then
       !! x is logP
       !! log P == lnP / ln(10)
       !! lnP == logP * ln10
       !! => dlnP == ln10 * dlogP
       dfdx = ln10 * d_dlnRho_c_T(which_other) / (d_dlnRho_c_T(i_lnPgas))
    else
       !! x is logT
       !! log P is constant
       !! dlnP = (d_dlnRho_c_T(i_lnPgas) * dlnRho + d_dlnT_c_Rho(i_lnPgas) * dlnT) = 0
       !!  dlnRho = -d_dlnT_c_Rho(i_lnPgas) * dlnT / d_dlnRho_c_T(i_lnPgas)
       !! d(other) | P constant 
       !!  = d_dlnRho_c_T(which_other) * dlnRho + d_dlnT_c_Rho(which_other) * dlnT
       !!  = [d_dlnRho_c_T(which_other) * (-d_dlnT_c_Rho(i_lnPgas) / d_dlnRho_c_T(i_lnPgas)) 
       !!        + d_dlnT_c_Rho(which_other)] * dlnT
       dfdx = ln10 * (d_dlnRho_c_T(which_other) * (-d_dlnT_c_Rho(i_lnPgas) / d_dlnRho_c_T(i_lnPgas)) &
                    + d_dlnT_c_Rho(which_other))
    end if
    
  end function iwPT_get_f_df
end module ideal_water
