
module watereos_def
  use const_def, only: dp

  implicit none
  
  logical, parameter :: mixdbg = .false.

  integer, parameter :: i_P = 1
  integer, parameter :: i_E = 2
  integer, parameter :: i_S = 3

  integer, parameter :: i_val = 1

  integer, parameter :: i_dRho = 2
  integer, parameter :: i_dlnRho = 2
  integer, parameter :: i_dlnP = 2
  integer, parameter :: i_dP = 2
  integer, parameter :: i_dX = 2
  integer, parameter :: i_dZ = 2
  
  integer, parameter :: i_dT = 3
  integer, parameter :: i_dlnT = 3
  integer, parameter :: i_dY = 3

  integer, parameter :: i_dRho2 = 4
  integer, parameter :: i_dlnRho2 = 4
  integer, parameter :: i_dlnP2 = 4
  integer, parameter :: i_dP2 = 4
  integer, parameter :: i_dX2 = 4
  integer, parameter :: i_dZ2 = 4

  integer, parameter :: i_dT2 = 5
  integer, parameter :: i_dlnT2 = 5
  integer, parameter :: i_dY2 = 5

  integer, parameter :: i_dRhodT = 6
  integer, parameter :: i_dPdT = 6
  integer, parameter :: i_dlnRhodlnT = 6
  integer, parameter :: i_dlnPdlnT = 6
  integer, parameter :: i_dXdY = 6
  integer, parameter :: i_dZdY = 6
  
  !! size of derivative vectors : for allocation
  integer, parameter :: num_derivs = 6
  
  real(dp), parameter :: logexp = log10(exp(1d1))


  integer, parameter :: IW_ERROR_OUTOFRANGE = -100
  real(dp), parameter :: watereos_tol = 1d-5
  integer, parameter :: watereos_maxiter = 100
end module watereos_def

