!! This is a sample implementation of an Analytic Water EOS to interface with MESA


module watereos_lib  
  use watereos_def  
  implicit none
  
contains
  
  subroutine water_eosDT_get( &       
       id, cell, eos_handle, Z, X, abar, zbar, & 
       species, chem_id, net_iso, xa, &
       Rho, log10Rho, T, log10T, & 
       res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
    
    use eos_def
    use chem_def, only: num_chem_isos
    use ideal_water

    ! INPUT
         
    integer, intent(in) :: id, cell, eos_handle 

    real(dp), intent(in) :: Z ! the metals mass fraction
    real(dp), intent(in) :: X ! the hydrogen mass fraction
            
    real(dp), intent(in) :: abar
    ! mean atomic number (nucleons per nucleus; grams per mole)
    real(dp), intent(in) :: zbar ! mean charge per nucleus
    
    integer, intent(in) :: species
    integer, pointer :: chem_id(:) ! maps species to chem id
    ! index from 1 to species
    ! value is between 1 and num_chem_isos         
    integer, pointer :: net_iso(:) ! maps chem id to species number
    ! index from 1 to num_chem_isos (defined in chem_def)
    ! value is 0 if the iso is not in the current net
    ! else is value between 1 and number of species in current net
    real(dp), intent(in) :: xa(:) ! mass fractions
         
    real(dp), intent(in) :: Rho, log10Rho ! the density
    ! provide both if you have them.  else pass one and set the other to arg_not_provided
    ! "arg_not_provided" is defined in mesa const_def
            
    real(dp), intent(in) :: T, log10T ! the temperature
    ! provide both if you have them.  else pass one and set the other to arg_not_provided
    
    ! OUTPUT
    
    real(dp), intent(out) :: res(:)
    ! partial derivatives of the basic results wrt lnd and lnT
    real(dp), intent(out) :: d_dlnRho_c_T(:) 
    ! d_dlnRho_c_T(i) = d(res(i))/dlnd|T
    real(dp), intent(out) :: d_dlnT_c_Rho(:) 
    ! d_dlnT(i) = d(res(i))/dlnT|Rho
         
    integer, intent(out) :: ierr ! 0 means AOK.
    real(dp) :: lnRho, lnT

    lnRho = log10Rho / logexp
    lnT = log10T / logexp

    call iw_get_res(Rho, lnRho, T, lnT, res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
!    write(*,*) "water_eosDT_get", log10Rho, log10T
  end subroutine water_eosDT_get


      
  subroutine water_eosDT_get_T( &
       id, cell, eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       logRho, which_other, other_value, &
       logT_tol, other_tol, max_iter, logT_guess, & 
       logT_bnd1, logT_bnd2, other_at_bnd1, other_at_bnd2, &
       logT_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
     
         ! finds log10 T given values for density and 'other', and initial guess for temperature.
         ! does up to max_iter attempts until logT changes by less than tol.
         
         ! 'other' can be any of the basic result variables for the eos
         ! specify 'which_other' by means of the definitions in eos_def (e.g., i_lnE)
         
    use eos_def
    use ideal_water
    use chem_def, only: num_chem_isos
    

    integer, intent(in) :: id, cell, eos_handle

    real(dp), intent(in) :: Z ! the metals mass fraction
    real(dp), intent(in) :: X ! the hydrogen mass fraction
    
    real(dp), intent(in) :: abar
    ! mean atomic number (nucleons per nucleus; grams per mole)
    real(dp), intent(in) :: zbar ! mean charge per nucleus
         
    integer, intent(in) :: species
    integer, pointer :: chem_id(:) ! maps species to chem id
    ! index from 1 to species
    ! value is between 1 and num_chem_isos         
    integer, pointer :: net_iso(:) ! maps chem id to species number
    ! index from 1 to num_chem_isos (defined in chem_def)
    ! value is 0 if the iso is not in the current net
    ! else is value between 1 and number of species in current net
    real(dp), intent(in) :: xa(:) ! mass fractions
    
    real(dp), intent(in) :: logRho ! log10 of density
    integer, intent(in) :: which_other ! from eos_def.  e.g., i_lnE
    real(dp), intent(in) :: other_value ! desired value for the other variable
    real(dp), intent(in) :: other_tol
    
    real(dp), intent(in) :: logT_tol
    integer, intent(in) :: max_iter ! max number of iterations        

    real(dp), intent(in) :: logT_guess ! log10 of temperature
    real(dp), intent(in) :: logT_bnd1, logT_bnd2 ! bounds for logT
    ! if don't know bounds, just set to arg_not_provided (defined in const_def)
    real(dp), intent(in) :: other_at_bnd1, other_at_bnd2 ! values at bounds
    ! if don't know these values, just set to arg_not_provided (defined in const_def)

    real(dp), intent(out) :: logT_result ! log10 of temperature
    real(dp), intent(out) :: res(:)
    real(dp), intent(out) :: d_dlnRho_c_T(:)
    real(dp), intent(out) :: d_dlnT_c_Rho(:)
    
    integer, intent(out) :: eos_calls
    integer, intent(out) :: ierr ! 0 means AOK.
    
    logT_result = 0
    res = 0
    d_dlnRho_c_T = 0
    d_dlnT_c_Rho = 0
    eos_calls = 0
    
    call iw_get_t(logRho, which_other, other_value, logT_tol, other_tol, max_iter, logT_guess, &
         logT_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)


  end subroutine water_eosDT_get_T
      

  subroutine water_eosDT_get_Rho( &
       id, cell, eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       logT, which_other, other_value, &
       logRho_tol, other_tol, max_iter, logRho_guess,  &
       logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2, &
       logRho_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, &
       ierr)
     
    use eos_def
    use ideal_water
    use chem_def, only: num_chem_isos

    ! finds log10 Rho given values for temperature and 'other', and initial guess for density.
    ! does up to max_iter attempts until logRho changes by less than tol.
    
    ! 'other' can be any of the basic result variables for the eos
    ! specify 'which_other' by means of the definitions in eos_def (e.g., i_lnE)
    
    integer, intent(in) :: id, cell, eos_handle
    
    real(dp), intent(in) :: Z ! the metals mass fraction
    real(dp), intent(in) :: X ! the hydrogen mass fraction
    
    real(dp), intent(in) :: abar
    ! mean atomic number (nucleons per nucleus; grams per mole)
    real(dp), intent(in) :: zbar ! mean charge per nucleus
    
    integer, intent(in) :: species
    integer, pointer :: chem_id(:) ! maps species to chem id
    ! index from 1 to species
    ! value is between 1 and num_chem_isos         
    integer, pointer :: net_iso(:) ! maps chem id to species number
    ! index from 1 to num_chem_isos (defined in chem_def)
    ! value is 0 if the iso is not in the current net
    ! else is value between 1 and number of species in current net
    real(dp), intent(in) :: xa(:) ! mass fractions
    
    real(dp), intent(in) :: logT ! log10 of temperature
    
    integer, intent(in) :: which_other ! from eos_def.  e.g., i_lnE
    real(dp), intent(in) :: other_value ! desired value for the other variable
    real(dp), intent(in) :: other_tol
    
    real(dp), intent(in) :: logRho_tol
    
    integer, intent(in) :: max_iter ! max number of Newton iterations        
    
    real(dp), intent(in) :: logRho_guess ! log10 of density
    real(dp), intent(in) :: logRho_bnd1, logRho_bnd2 ! bounds for logRho
    ! if don't know bounds, just set to arg_not_provided (defined in const_def)
    real(dp), intent(in) :: other_at_bnd1, other_at_bnd2 ! values at bounds
    ! if don't know these values, just set to arg_not_provided (defined in const_def)
    
    real(dp), intent(out) :: logRho_result ! log10 of density
    
    real(dp), intent(out) :: res(:)
    real(dp), intent(out) :: d_dlnRho_c_T(:)
    real(dp), intent(out) :: d_dlnT_c_Rho(:)
    
    integer, intent(out) :: eos_calls
    integer, intent(out) :: ierr ! 0 means AOK.
    
    logRho_result = 0
    res = 0
    d_dlnRho_c_T = 0
    d_dlnT_c_Rho = 0
    eos_calls = 0

    call iw_get_rho(logT, which_other, other_value, &
         logRho_tol, other_tol, max_iter, logRho_guess, &
         logRho_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)

  end subroutine water_eosDT_get_Rho

        
  ! the following routine uses gas pressure and temperature as input variables
  subroutine water_eosPT_get(&
       id, cell, eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa,&
       Pgas, log10Pgas, T, log10T, &
       Rho, log10Rho, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas, &
       res, d_dlnRho_const_T, d_dlnT_const_Rho, ierr)
    
    use eos_def
    use chem_def, only: num_chem_isos
    use ideal_water
    
    ! INPUT
    
    integer, intent(in) :: id, cell, eos_handle
    
    real(dp), intent(in) :: Z ! the metals mass fraction
    real(dp), intent(in) :: X ! the hydrogen mass fraction
    
    real(dp), intent(in) :: abar
    ! mean atomic number (nucleons per nucleus; grams per mole)
    real(dp), intent(in) :: zbar ! mean charge per nucleus
    
    integer, intent(in) :: species
    integer, pointer :: chem_id(:) ! maps species to chem id
    ! index from 1 to species
    ! value is between 1 and num_chem_isos         
    integer, pointer :: net_iso(:) ! maps chem id to species number
    ! index from 1 to num_chem_isos (defined in chem_def)
    ! value is 0 if the iso is not in the current net
    ! else is value between 1 and number of species in current net
    real(dp), intent(in) :: xa(:) ! mass fractions
    
    real(dp), intent(in) :: Pgas, log10Pgas ! the gas pressure
    ! provide both if you have them.  else pass one and set the other to arg_not_provided
    ! "arg_not_provided" is defined in mesa const_def
    
    real(dp), intent(in) :: T, log10T ! the temperature
    ! provide both if you have them.  else pass one and set the other to arg_not_provided
    
    ! OUTPUT
    
    real(dp), intent(out) :: Rho, log10Rho ! density
    real(dp), intent(out) :: dlnRho_dlnPgas_const_T
    real(dp), intent(out) :: dlnRho_dlnT_const_Pgas
    real(dp), intent(out) :: res(:)
    ! partial derivatives of the basic results wrt lnd and lnT
    real(dp), intent(out) :: d_dlnRho_const_T(:) 
    ! d_dlnRho_const_T(i) = d(res(i))/dlnd|T
    real(dp), intent(out) :: d_dlnT_const_Rho(:) 
    ! d_dlnT_const_Rho(i) = d(res(i))/dlnT|Rho
    
    integer, intent(out) :: ierr ! 0 means AOK.
    
    Rho = 0
    log10Rho = 0
    dlnRho_dlnPgas_const_T = 0
    dlnRho_dlnT_const_Pgas = 0
    res = 0
    d_dlnRho_const_T = 0
    d_dlnT_const_Rho = 0
    
    call iwPT_get_res(Pgas, log10Pgas, T, log10T, Rho, log10Rho, &
         res, d_dlnRho_const_T, d_dlnT_const_Rho, ierr)
    dlnRho_dlnPgas_const_T = 1d0 / d_dlnRho_const_T(i_lnPgas)
    dlnRho_dlnT_const_Pgas = - d_dlnT_const_Rho(i_lnPgas) / d_dlnRho_const_T(i_lnPgas)
    if(ierr .ne. 0) then
       write(*,*) "water_eosPT_get", log10Pgas, log10T, ierr
    endif
  end subroutine water_eosPT_get
      

  ! eosPT search routines
  
  subroutine water_eosPT_get_T( &
       id, cell, eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa,&
       logPgas, which_other, other_value,&
       logT_tol, other_tol, max_iter, logT_guess, &
       logT_bnd1, logT_bnd2, other_at_bnd1, other_at_bnd2,&
       logT_result, Rho, log10Rho, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas, &
       res, d_dlnRho_const_T, d_dlnT_const_Rho, eos_calls, ierr)
    
    ! finds log10 T given values for gas pressure and 'other',
    ! and initial guess for temperature.
    ! does up to max_iter attempts until logT changes by less than tol.
    
    ! 'other' can be any of the basic result variables for the eos
    ! specify 'which_other' by means of the definitions in eos_def (e.g., i_lnE)
    
    use chem_def, only: num_chem_isos
    use eos_def
    use ideal_water
         
    integer, intent(in) :: id, cell, eos_handle
    
    real(dp), intent(in) :: Z ! the metals mass fraction
    real(dp), intent(in) :: X ! the hydrogen mass fraction
    
    real(dp), intent(in) :: abar
    ! mean atomic number (nucleons per nucleus; grams per mole)
    real(dp), intent(in) :: zbar ! mean charge per nucleus
    
    integer, intent(in) :: species
    integer, pointer :: chem_id(:) ! maps species to chem id
    ! index from 1 to species
    ! value is between 1 and num_chem_isos         
    integer, pointer :: net_iso(:) ! maps chem id to species number
    ! index from 1 to num_chem_isos (defined in chem_def)
    ! value is 0 if the iso is not in the current net
    ! else is value between 1 and number of species in current net
    real(dp), intent(in) :: xa(:) ! mass fractions
    
    real(dp), intent(in) :: logPgas ! log10 of gas pressure
    integer, intent(in) :: which_other ! from eos_def.  e.g., i_lnE
    real(dp), intent(in) :: other_value ! desired value for the other variable
    real(dp), intent(in) :: other_tol
    
    real(dp), intent(in) :: logT_tol
    integer, intent(in) :: max_iter ! max number of iterations        
    
    real(dp), intent(in) :: logT_guess ! log10 of temperature
    real(dp), intent(in) :: logT_bnd1, logT_bnd2 ! bounds for logT
    ! if don't know bounds, just set to arg_not_provided (defined in const_def)
    real(dp), intent(in) :: other_at_bnd1, other_at_bnd2 ! values at bounds
    ! if don't know these values, just set to arg_not_provided (defined in const_def)
    
    real(dp), intent(out) :: logT_result ! log10 of temperature
    real(dp), intent(out) :: Rho, log10Rho ! density
    real(dp), intent(out) :: dlnRho_dlnPgas_const_T
    real(dp), intent(out) :: dlnRho_dlnT_const_Pgas
    
    real(dp), intent(out) :: res(:)
    real(dp), intent(out) :: d_dlnRho_const_T(:)
    real(dp), intent(out) :: d_dlnT_const_Rho(:)
    
    real(dp) :: P, T
    integer, intent(out) :: eos_calls
    integer, intent(out) :: ierr ! 0 means AOK.
    
    logT_result = 0
    Rho = 0
    log10Rho = 0
    dlnRho_dlnPgas_const_T = 0
    dlnRho_dlnT_const_Pgas = 0
    res = 0
    d_dlnRho_const_T = 0
    d_dlnT_const_Rho = 0
    eos_calls = 0
    
    call iwPT_get_T(logPgas, which_other, other_value, &
         logT_tol, other_tol, max_iter, &
         logT_result, res, d_dlnRho_const_T, d_dlnT_const_Rho, eos_calls, ierr)
    P = 1d1**logPgas
    T = 1d1**logT_result
    call iwRho_PT(P, T, Rho)
    log10Rho = log10(Rho)
  end subroutine water_eosPT_get_T
      
  
  subroutine water_eosPT_get_Pgas(&
       id, cell, eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa,&
       logT, which_other, other_value,&
       logPgas_tol, other_tol, max_iter, logPgas_guess, &
       logPgas_bnd1, logPgas_bnd2, other_at_bnd1, other_at_bnd2,&
       logPgas_result, Rho, log10Rho, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas, &
       res, d_dlnRho_const_T, d_dlnT_const_Rho, eos_calls, ierr)
    
         ! finds log10 Pgas given values for temperature and 'other', and initial guess for gas pressure.
         ! does up to max_iter attempts until logPgas changes by less than tol.
         
         ! 'other' can be any of the basic result variables for the eos
         ! specify 'which_other' by means of the definitions in eos_def (e.g., i_lnE)
         
    use chem_def, only: num_chem_isos
    use eos_def
    use ideal_water
         
    integer, intent(in) :: id, cell, eos_handle

    real(dp), intent(in) :: Z ! the metals mass fraction
    real(dp), intent(in) :: X ! the hydrogen mass fraction
            
    real(dp), intent(in) :: abar
    ! mean atomic number (nucleons per nucleus; grams per mole)
    real(dp), intent(in) :: zbar ! mean charge per nucleus
         
    integer, intent(in) :: species
    integer, pointer :: chem_id(:) ! maps species to chem id
    ! index from 1 to species
    ! value is between 1 and num_chem_isos         
    integer, pointer :: net_iso(:) ! maps chem id to species number
    ! index from 1 to num_chem_isos (defined in chem_def)
    ! value is 0 if the iso is not in the current net
    ! else is value between 1 and number of species in current net
    real(dp), intent(in) :: xa(:) ! mass fractions
    
    real(dp), intent(in) :: logT ! log10 of temperature
    
    integer, intent(in) :: which_other ! from eos_def.  e.g., i_lnE
    real(dp), intent(in) :: other_value ! desired value for the other variable
    real(dp), intent(in) :: other_tol
    
    real(dp), intent(in) :: logPgas_tol
    
    integer, intent(in) :: max_iter ! max number of Newton iterations        
    
    real(dp), intent(in) :: logPgas_guess ! log10 of gas pressure
    real(dp), intent(in) :: logPgas_bnd1, logPgas_bnd2 ! bounds for logPgas
    ! if don't know bounds, just set to arg_not_provided (defined in const_def)
    real(dp), intent(in) :: other_at_bnd1, other_at_bnd2 ! values at bounds
    ! if don't know these values, just set to arg_not_provided (defined in const_def)
    
    real(dp), intent(out) :: logPgas_result ! log10 of gas pressure
    real(dp), intent(out) :: Rho, log10Rho ! density
    real(dp), intent(out) :: dlnRho_dlnPgas_const_T
    real(dp), intent(out) :: dlnRho_dlnT_const_Pgas
    
    real(dp), intent(out) :: res(:)
    real(dp), intent(out) :: d_dlnRho_const_T(:)
    real(dp), intent(out) :: d_dlnT_const_Rho(:)

    real(dp) :: P, T
    
    integer, intent(out) :: eos_calls
    integer, intent(out) :: ierr ! 0 means AOK.
    
    logPgas_result = 0
    Rho = 0
    log10Rho = 0
    dlnRho_dlnPgas_const_T = 0
    dlnRho_dlnT_const_Pgas = 0
    res = 0
    d_dlnRho_const_T = 0
    d_dlnT_const_Rho = 0
    eos_calls = 0
    
    call iwPT_get_P(logT, which_other, other_value, &
         logPgas_tol, other_tol, max_iter, &
         logPgas_result, &
         res, d_dlnRho_const_T, d_dlnT_const_Rho, &
         eos_calls, ierr)
    
    P = 1d1**logPgas_result
    T = 1d1**logT
    call iwRho_PT(P, T, Rho)
    log10Rho = log10(Rho)
  end subroutine water_eosPT_get_Pgas
  


  !! In principle, this is a different function.  For our situation, 
  !! this is the same as water_eos_DT_get
  subroutine water_eosPT_get_Pgas_for_Rho(&
       id, cell, eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa,&
       logT, logRho_want,&
       logPgas_tol, logRho_tol, max_iter, logPgas_guess, &
       logPgas_bnd1, logPgas_bnd2, logRho_at_bnd1, logRho_at_bnd2,&
       logPgas_result, Rho, logRho, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas, &
       res, d_dlnRho_const_T, d_dlnT_const_Rho, eos_calls, ierr)
    
    ! finds log10 Pgas given values for temperature and density, and initial guess for gas pressure.
    ! does up to max_iter attempts until logPgas changes by less than tol.
    
    use const_def, only: ln10
    use chem_def, only: num_chem_isos         
    use eos_def
    use ideal_water
    
    integer, intent(in) :: id, cell, eos_handle
    
    real(dp), intent(in) :: Z ! the metals mass fraction
    real(dp), intent(in) :: X ! the hydrogen mass fraction
    
    real(dp), intent(in) :: abar
    ! mean atomic number (nucleons per nucleus; grams per mole)
    real(dp), intent(in) :: zbar ! mean charge per nucleus
    
    integer, intent(in) :: species
    integer, pointer :: chem_id(:) ! maps species to chem id
    ! index from 1 to species
    ! value is between 1 and num_chem_isos         
    integer, pointer :: net_iso(:) ! maps chem id to species number
    ! index from 1 to num_chem_isos (defined in chem_def)
    ! value is 0 if the iso is not in the current net
    ! else is value between 1 and number of species in current net
    real(dp), intent(in) :: xa(:) ! mass fractions
    
    real(dp), intent(in) :: logT ! log10 of temperature
    
    real(dp), intent(in) :: logRho_want ! log10 of desired density
    real(dp), intent(in) :: logRho_tol
    
    real(dp), intent(in) :: logPgas_tol
    
    integer, intent(in) :: max_iter ! max number of Newton iterations        
    
    real(dp), intent(in) :: logPgas_guess ! log10 of gas pressure
    real(dp), intent(in) :: logPgas_bnd1, logPgas_bnd2 ! bounds for logPgas
    ! if don't know bounds, just set to arg_not_provided (defined in const_def)
    real(dp), intent(in) :: logRho_at_bnd1, logRho_at_bnd2 ! values at bounds
    ! if don't know these values, just set to arg_not_provided (defined in const_def)
    
    real(dp), intent(out) :: logPgas_result ! log10 of gas pressure
    real(dp), intent(out) :: Rho, logRho ! density corresponding to logPgas_result
    real(dp), intent(out) :: dlnRho_dlnPgas_const_T
    real(dp), intent(out) :: dlnRho_dlnT_const_Pgas
    
    real(dp), intent(out) :: res(:)
    real(dp), intent(out) :: d_dlnRho_const_T(:)
    real(dp), intent(out) :: d_dlnT_const_Rho(:)
    
    integer, intent(out) :: eos_calls
    integer, intent(out) :: ierr ! 0 means AOK.
    
    real(dp) :: T
    
    T = 1d1**logT
    Rho = 1d1**logRho_want
    logRho = logRho_want
    
    logPgas_result = 0d0
    dlnRho_dlnPgas_const_T = 0d0
    dlnRho_dlnT_const_Pgas = 0d0
    res = 0
    d_dlnRho_const_T = 0
    d_dlnT_const_Rho = 0
    eos_calls = 0
    
    call iw_get_res(Rho, logRho, T, logT, &
         res, d_dlnRho_const_T, d_dlnT_const_Rho, ierr)
    logPgas_result = res(i_lnPgas) / ln10
    dlnRho_dlnPgas_const_T = 1d0 / d_dlnRho_const_T(i_lnPgas)
    dlnRho_dlnT_const_Pgas = - d_dlnT_const_Rho(i_lnPgas) / d_dlnRho_const_T(i_lnPgas)
  end subroutine water_eosPT_get_Pgas_for_Rho
  
end module watereos_lib

