module test_watereos_mod
  use watereos_def
  use watereos_lib
  use eos_def
  use eos_lib
  
  implicit none
  
  integer, parameter :: species = 5 !! Arbitrary since xa won't be called

  integer :: id, cell, handle
  real(dp) :: Z, X, abar, zbar
  integer, pointer :: chem_id(:), net_iso(:)
  real(dp) :: xa(species)
  
contains

  subroutine do_test
    use const_def, only: arg_not_provided
    use eos_def
    real(dp) :: Rho, log10Rho, T, log10T, Pgas, logPgas
    real(dp) :: res(num_eos_basic_results), &
         d_dlnRho_c_T(num_eos_basic_results), &
         d_dlnT_c_Rho(num_eos_basic_results)
    integer :: ierr
    integer :: i
    real(dp) :: logRho_tol, logT_tol, other_tol, logPgas_tol
    integer :: max_iter, which_other, eos_calls
    real(dp) :: logRho_guess, logT_guess, logPgas_guess, other_value, &
         logRho_bnd1, logRho_bnd2, logT_bnd1, logT_bnd2, other_at_bnd1, other_at_bnd2, &
         logPgas_bnd1, logPgas_bnd2, &
         Rho_result, logRho_result, logT_result, logPgas_result, &
         dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas
    real(dp) :: RhoInput, RhoOutput, TInput, TOutput
    real(dp) :: logTMin, logTMax, dlogT
    integer :: nstep = 10

    logTMin = 2d0
    logTMax = 9d0
    dlogT = (logTMax - logTMin) / (nstep-1)
    

    !! TODO, nested loops.  Perform these tests of Rho and T space 
    do i=1,nstep
       log10T = logTMin + (i-1) * dlogT
       T = 1d1**log10T
       TInput = T       
    
       log10Rho = 0d0
       Rho = 1d1**log10Rho
       RhoInput = Rho

       call water_eosDT_get(id, cell, handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            Rho, log10Rho, T, log10T, &
            res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
!       call print_res(res, log10Rho, log10T)
       
       logPgas = res(i_lnPgas) / log(1d1)
       which_other = i_lnS
       other_value = res(i_lnS)
       
       logT_bnd1 = arg_not_provided
       logT_bnd2 = arg_not_provided
       other_at_bnd1 = arg_not_provided
       other_at_bnd2 = arg_not_provided
       logT_tol = 1d-10
       other_tol = 1d-10
       max_iter = 200
       logT_guess = 5.d0
       
!    write(*,*) "logPgas = ", logPgas
!    write(*,*) "logTguess = ", logT_guess
!    write(*,*) "other value = ", other_value
!    write(*,*) "which other = ", which_other
       call water_eosDT_get_T(id, cell, handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            log10Rho, which_other, other_value, &
            logT_tol, other_tol, max_iter, logT_guess, &
            logT_bnd1, logT_bnd2, other_at_bnd1, other_at_bnd2, &
            logT_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)

       if(abs(log10T - logT_result) .gt. logT_tol) then
          write(*,*) "eosDT_get_T test FAILED at ", log10Rho, log10T
          write(*,*) "logT " , log10T, logT_result , log10T - logT_result, eos_calls, ierr
          call print_res(res, log10Rho, logT_result)
       endif

       logRho_tol = 1d-10
       logRho_guess = 1d0
       logRho_bnd1 = arg_not_provided
       logRho_bnd2 = arg_not_provided
       call water_eosDT_get_Rho(id, cell, handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            log10T, which_other, other_value, &
            logRho_tol, other_tol, max_iter, logRho_guess, &
            logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2, &
            logRho_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, &
            ierr)
       if(abs(log10Rho - logRho_result) .gt. logRho_tol) then
          write(*,*) "eosDT_get_Rho test FAILED at ", log10Rho, log10T
          write(*,*) "logRho ", log10Rho, logRho_result, log10Rho - logRho_result, eos_calls, ierr
          call print_res(res, logRho_result, log10T)
       endif

       Pgas = 1d1**logPgas
       call water_eosPT_get(id, cell, handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            Pgas, logPgas, T, log10T, &
            Rho_result, logRho_result, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas, &
            res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr) !! define params
       if(abs(log10Rho - logRho_result) .gt. logRho_tol) then
          write(*,*) "eosPT_get results test FAILED at ", log10Rho, log10T
          write(*,*) "logRho ", log10Rho, logRho_result, log10Rho - logRho_result, eos_calls, ierr
          call print_res(res, logRho_result, log10T)
       endif
       

       logPgas_bnd1 = arg_not_provided
       logPgas_bnd2 = arg_not_provided
       logPgas_tol = 1d-10
       logPgas_guess = 1d10
       call water_eosPT_get_Pgas(id, cell, handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            log10T, which_other, other_value, &
            logPgas_tol, other_tol, max_iter, logPgas_guess, &
            logPgas_bnd1, logPgas_bnd2, other_at_bnd1, other_at_bnd2, &
            logPgas_result, Rho_result, logRho_result, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas, &
            res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
       if(abs(logPgas_result - logPgas) .gt. logPgas_tol) then
          write(*,*) "eosPT_get_Pgas test FAILED at ", log10Rho, log10T
          write(*,*) "logPgas: ", logPgas, logPgas_result, logPgas - logPgas_result, eos_calls, ierr
          call print_res(res, logRho_result, log10T)
       endif

       call water_eosPT_get_T(id, cell, handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            logPgas, which_other, other_value, &
            logT_tol, other_tol, max_iter, logT_guess, &
            logT_bnd1, logT_bnd2, other_at_bnd1, other_at_bnd2, &
            logT_result, Rho, log10Rho, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas, &
            res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
       
       RhoOutput = Rho
       TOutput = 1d1**logT_result       
       if((ierr .ne. 0) .or. (abs(RhoInput - RhoOutput)/RhoInput .gt. 1d-3) .or. &
            abs(TInput - TOutput)/TInput .gt. 1d-3) then
          write(*,*) "eosPT_get_T test FAILED at ", log10Rho, log10T
          write(*,*) i, eos_calls, ierr
          write(*,*) "Rho : ", RhoInput, RhoOutput
          write(*,*) "T   : ", TInput, TOutput
          call print_res(res, log10Rho, logT_result)
       endif
    end do
  end subroutine do_test
  
  subroutine test_build_model
    use const_def, only: pi, standard_cgrav
    use eos_def
    integer, parameter :: NCELL = 200
    real(dp) :: PGrid(NCELL), TGrid(NCELL), RhoGrid(NCELL), &
         MassGrid(NCELL), dMGrid(NCELL), RadiusGrid(NCELL), lnSGrid(NCELL)
    real(dp) :: Pouter = 1d6, &
         Mp = 2d33, & ! Mass of Neptune
         RhoGuess = 1d0, &
         TGuess = 1d9, &
         lnS_model = 21.0d0, &
         CoreMass = 0d0, &
         CoreDensity = 1d0, &
         CoreRadius

    character (len=256) :: filename
    integer :: fid = 20
    
    !! (4/3) pi r^3 * Rho = Mass
    !!    r = ((3/4) * Mass / Rho)^(1/3) 
    
    integer :: i
    integer :: iter, num_iter = 500, ierr
    
    real(dp) :: Rho, log10Rho, T, log10T, plRad
    real(dp) :: res(num_eos_basic_results), d_dlnRho_c_T(num_eos_basic_results), &
         d_dlnT_c_Rho(num_eos_basic_results)

    CoreRadius = ((3./4.) * CoreMass / CoreDensity)**(1d0/3d0)
    
    !! Setup the initial mass profile
    do i=1,NCELL
       MassGrid(i) = (-cos(real(i-1) * pi / (NCELL-1)) + 1d0) * 0.5 * (Mp - CoreMass) + CoreMass 
       if(i .gt. 1) then
          dMGrid(i-1) = MassGrid(i) - MassGrid(i-1)
       endif
       RhoGrid(i) = RhoGuess
       RadiusGrid(i) = (MassGrid(i) / (RhoGuess * 4d0*pi/3d0))**(1d0/3d0)    
       TGrid(i) = TGuess
       
       Rho = RhoGrid(i)
       log10Rho = log10(Rho)
       T = TGrid(i)
       log10T = log10(T)
       
       call water_eosDT_get(id, cell, handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            Rho, log10Rho, T, log10T, &
            res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
       
       PGrid(i) = exp(res(i_lnPgas))
    enddo
    !! Figure out an initial profile by iterating
    
    do iter=1,num_iter
       call update_pressure
       call update_density
    end do
    
    do i=1,NCELL
       Rho = RhoGrid(i)
       log10Rho = log10(RhoGrid(i))
       T = TGrid(i)
       log10T = log10(T)
       call water_eosDT_get(id, cell, handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            Rho, log10Rho, T, log10T, &
            res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
       lnSGrid(i) = res(i_lnS)
   
    enddo

    filename = "atm.dat"
    open(unit=fid, file=filename)

    do i=1,NCELL
       write(fid,*) i, MassGrid(i), RadiusGrid(i), RhoGrid(i), PGrid(i), TGrid(i), lnSGrid(i)
    enddo
    close(unit=fid)

    plRad = RadiusGrid(NCELL)

    write(*,*) "The central pressure should be roughly: " , &
         standard_cgrav * Mp**2d0 / (8 * pi * plRad**4d0)

  contains

    subroutine update_density
      use chem_def, only: num_chem_isos
      use eos_def
      use watereos_lib
      use const_def, only: arg_not_provided

      integer :: which_other, max_iter
      real(dp) :: Rho, log10Rho, logT_result, &
           d_dlnRho_dlnPgas_const_T, d_dlnRho_const_Pgas, &
           logPgas, other_value, logT_tol, other_tol, logT_guess, dv, &
           logT_bnd1, logT_bnd2, other_at_bnd1, other_at_Bnd2
      real(dp) :: res(num_eos_basic_results), d_dlnRho_const_T(num_eos_basic_results), &
                  d_dlnT_const_Rho(num_eos_basic_results)
      integer :: eos_calls, ierr
      
      
      !! 1. Determine density from pressure and fixed entropy
      
      do i=1,NCELL
         
         other_value = lnS_model
         which_other = i_lnS
         max_iter = 100
         logPgas = log10(PGrid(i))
         logT_guess = log10(TGrid(i))
         logT_bnd1 = arg_not_provided
         logT_bnd2 = arg_not_provided
         other_at_bnd1 = arg_not_provided
         other_at_bnd2 = arg_not_provided
         logT_tol = 1d-10
         other_tol = 1d-10

         call water_eosPT_get_T(id, cell, handle, Z, X, abar, zbar, &
              species, chem_id, net_iso, xa, &
              logPgas, which_other, other_value, &
              logT_tol, other_tol, max_iter, logT_guess, &
              logT_bnd1, logT_bnd2, other_at_bnd1, other_at_bnd2, &
              logT_result, Rho, log10Rho, d_dlnRho_dlnPgas_const_T, d_dlnRho_const_Pgas, &
              res, d_dlnRho_const_T, d_dlnT_const_Rho, eos_calls, ierr)
         
         if(ierr.ne.0) then
            write(*,*) logPgas, logT_guess, logT_result, eos_calls, ierr
         endif
         RhoGrid(i) = Rho
         TGrid(i) = 1d1**logT_result
         
      enddo

      !! 2. Determine Radius from density

      RadiusGrid(1) = CoreRadius
      do i=2,NCELL 
         dv = (dMGrid(i-1) / ((RhoGrid(i) + RhoGrid(i-1))/2d0))
!         write(*,*) dMGrid(i-1), RhoGrid(i), dv
         RadiusGrid(i) = (RadiusGrid(i-1)**3d0 + (3d0/(4d0*pi))*dv)**(1d0/3d0)
         
      end do
    end subroutine update_density

    
    subroutine update_pressure
      PGrid(NCELL) = Pouter
      do i=NCELL-1,2,-1
         PGrid(i) = Pgrid(i+1) + 2d0*standard_cgrav * (MassGrid(i+1)**2d0 - MassGrid(i)**2d0) &
              / (pi * (RadiusGrid(i+1) + RadiusGrid(i))**4d0)
      enddo
      Pgrid(1) = PGrid(2) + (4d0*pi/6d0) * standard_cgrav * (RhoGrid(1)*RadiusGrid(2))**2d0      
    end subroutine update_pressure


  end subroutine test_build_model
  
  subroutine print_res(res, log10Rho, log10T)
    real(dp), intent(in) :: res(num_eos_basic_results)
    real(dp), intent(in) :: log10Rho, log10T
    integer :: i
    
    write(*,*) "log10Rho = ", log10Rho
    write(*,*) "log10T   = ", log10T
    write(*,*) "Results vector "
    do i=1,num_eos_basic_results
       write(*,*) eosDT_result_names(i), res(i)
    enddo
    
  end subroutine print_res
  
end module test_watereos_mod


program test_eos
  use test_watereos_mod
  use eos_lib
  use const_lib
  implicit none
  
  character (len=256) :: data_dir, eos_file_prefix
  logical :: use_cache
  integer :: info, ierr
  
  data_dir = "/Users/neil/research/mesa/data/"
  eos_file_prefix = "/Users/neil/research/mesa/"
  use_cache = .false.
  
  call const_init(eos_file_prefix, ierr)
  
  call eos_init( eos_file_prefix, use_cache, info)
  
  call do_test
  
  call test_build_model
end program test_eos
